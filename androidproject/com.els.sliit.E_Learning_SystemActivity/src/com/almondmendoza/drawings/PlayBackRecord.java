package com.almondmendoza.drawings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
//import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import chat.MessageThread;

import com.almondmendoza.drawings.brush.Brush;
import com.almondmendoza.drawings.brush.CircleBrush;
import com.almondmendoza.drawings.brush.PenBrush;
import com.els.sliit.ChatTeach;
import com.els.sliit.Dashboard;
import com.els.sliit.GlobalClass;
import com.els.sliit.Login;
import com.els.sliit.R;
import com.els.webservices.dbAccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.*;
import org.jivesoftware.smack.packet.*;
import org.jivesoftware.smack.util.StringUtils;


/**
 * DrawingActivity
 * Date: 05/17/2012
 * 
 */
public class PlayBackRecord extends Activity implements View.OnTouchListener{
	private DrawingSurface drawingSurface;
	private DrawingPath currentDrawingPath;
	private Paint currentPaint;

	private Button redoBtn;
	private Button undoBtn;

	private Brush currentBrush;

	ArrayList<String> valueArray = new ArrayList<String>();
	String temp ="[1#350.0#293.0, 2#350.0#291.0, 2#353.0#291.0, 2#363.0#293.0, 2#381.0#298.0, 2#413.0#301.0, 2#429.0#304.0, 2#434.0#306.0, 2#433.0#308.0, 2#423.0#311.0, 2#409.0#314.0, 2#400.0#318.0, 2#386.0#323.0, 2#377.0#324.0, 2#339.0#354.0, 2#334.0#362.0, 2#332.0#370.0, 2#335.0#397.0, 2#337.0#403.0, 2#342.0#416.0, 2#350.0#423.0, 2#363.0#430.0, 2#381.0#436.0, 2#416.0#444.0, 2#470.0#451.0, 2#497.0#449.0, 2#515.0#446.0, 2#533.0#436.0, 2#536.0#431.0, 2#540.0#425.0, 2#546.0#408.0, 2#551.0#395.0, 2#553.0#382.0, 2#556.0#370.0, 2#558.0#364.0, 2#559.0#356.0, 2#559.0#342.0, 2#559.0#331.0, 2#556.0#323.0, 2#548.0#306.0, 2#540.0#295.0, 2#525.0#285.0, 2#490.0#260.0, 2#454.0#240.0, 2#418.0#222.0, 2#373.0#202.0, 2#357.0#197.0, 2#339.0#192.0, 2#325.0#192.0, 2#304.0#192.0, 2#297.0#196.0, 2#293.0#197.0, 2#291.0#199.0, 3#291.0#199.0]";
	/* 	Create Unique name using current system date and time */
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	String curentDateandTime = sdf.format(new Date());

	//record code
	SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
	String CurrTime = sdf2.format(new Date());


	//Set the path to store the whiteboard image in SDcard
	private File APP_FILE_PATH = new File("/sdcard/elearn/wb_history");
	private ArrayList<String> m_group;
	private Menu m_people;
	private String recordFileName;
	private boolean isRecording;
	private String touchDownTime;
	private String touchUpTime;




	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawing_activity);

		setCurrentPaint();
		currentBrush = new PenBrush();


		drawingSurface = (DrawingSurface) findViewById(R.id.drawingSurface);

		drawingSurface.setOnTouchListener(this);

		redoBtn = (Button) findViewById(R.id.redoBtn);
		undoBtn = (Button) findViewById(R.id.undoBtn);

		redoBtn.setEnabled(false);
		undoBtn.setEnabled(false);

		redoBtn.setVisibility(View.GONE);
		undoBtn.setVisibility(View.GONE);

		Button btn1 = (Button) findViewById(R.id.colorRedBtn);
		Button btn2 = (Button) findViewById(R.id.colorBlueBtn);
		Button btn3 = (Button) findViewById(R.id.saveBtn);
		Button btn4 = (Button) findViewById(R.id.circleBtn);
		Button btn5 = (Button) findViewById(R.id.pathBtn);
		Button btn6 = (Button) findViewById(R.id.eraser);
		Button btn7 = (Button) findViewById(R.id.colorGreenBtn);
		Button btn8 = (Button) findViewById(R.id.toggleBtn);

		btn1.setVisibility(View.GONE);
		btn2.setVisibility(View.GONE);
		btn3.setVisibility(View.GONE);
		btn4.setVisibility(View.GONE);
		btn5.setVisibility(View.GONE);
		btn6.setVisibility(View.GONE);
		btn7.setVisibility(View.GONE);
		btn8.setVisibility(View.GONE);

		Button btnRec = (Button) findViewById(R.id.btnRec);

		btnRec.setText("|> Play");

	}


	// recording code



	///// end recording code /////////////

	public void makeRed()
	{
		currentPaint = new Paint();
		currentPaint.setDither(true);
		currentPaint.setColor(0xFFFF0000);
		currentPaint.setStyle(Paint.Style.STROKE);
		currentPaint.setStrokeJoin(Paint.Join.ROUND);
		currentPaint.setStrokeCap(Paint.Cap.ROUND);
		currentPaint.setStrokeWidth(3);
	}

	public void makeGreen()
	{

		currentPaint = new Paint();
		currentPaint.setDither(true);
		currentPaint.setColor(0xFF00FF00);
		currentPaint.setStyle(Paint.Style.STROKE);
		currentPaint.setStrokeJoin(Paint.Join.ROUND);
		currentPaint.setStrokeCap(Paint.Cap.ROUND);
		currentPaint.setStrokeWidth(3);
	}

	public void makeBlue()
	{
		currentPaint = new Paint();
		currentPaint.setDither(true);
		currentPaint.setColor(0xFF0000FF);
		currentPaint.setStyle(Paint.Style.STROKE);
		currentPaint.setStrokeJoin(Paint.Join.ROUND);
		currentPaint.setStrokeCap(Paint.Cap.ROUND);
		currentPaint.setStrokeWidth(3);

	}

	public void makeErase()
	{
		currentPaint = new Paint();
		currentPaint.setDither(true);
		currentPaint.setColor(Color.WHITE); //Bcoz background color is also white
		currentPaint.setStyle(Paint.Style.STROKE);
		currentPaint.setStrokeJoin(Paint.Join.ROUND);
		currentPaint.setStrokeCap(Paint.Cap.ROUND);
		currentPaint.setStrokeWidth(20);
	}

	public void makePath()
	{
		currentBrush = new PenBrush();
	}

	public void makeCircle()
	{
		currentBrush = new CircleBrush();
	}

	public void makeUndo()
	{
		drawingSurface.myundo();
		if( drawingSurface.myhasMoreRedo() == false ){
			undoBtn.setEnabled( false );
		}
		redoBtn.setEnabled( true );
	}

	public void makeRedo()
	{
		drawingSurface.myredo();
		if( drawingSurface.myhasMoreRedo() == false ){
			redoBtn.setEnabled( false );
		}

		undoBtn.setEnabled( true );
	}


	/*create a Paint that would serve as our current paint, 
	 * and set the currentPaint 
	 * at the start of our activity.*/
	private void setCurrentPaint(){
		currentPaint = new Paint();
		currentPaint.setDither(true);//Dithering affects how colors that are higher precision than the device are down-sampled.
		currentPaint.setColor(0xFFFFFF00); //color
		currentPaint.setStyle(Paint.Style.STROKE); //Geometry and text drawn with this style will be stroked, respecting the stroke-related fields on the paint. 
		currentPaint.setStrokeJoin(Paint.Join.ROUND); //The outer edges of a join meet in a circular arc. 
		currentPaint.setStrokeCap(Paint.Cap.ROUND); //The stroke projects out as a semicircle, with the center at the end of the path. 
		currentPaint.setStrokeWidth(3);

	}

	//record play back
	//VIDEO_20120721_104859
	public void playBack(String fileName)
	{
		String recData = readRecordedFile(fileName);
		addPaths(recData);

	}

	public String readRecordedFile(String fileName)
	{

		File sdcard = Environment.getExternalStorageDirectory();

		//Get the text file
		File file = new File(sdcard,fileName+".rec");

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);

			}
		}
		catch (IOException e) {
			//You'll need to add proper error handling here
		}

		return text.toString();

	}

	public void addPaths(String paths){




		String msg[] = paths.split(">");

		//1

		for(int r = 1 ; r < msg.length ; r++)
		{

			String path[] = msg[r].split("<");

			String StartTime = path[0];
			String pathArrayString = path[1];
			String EndTime = path[2];

			if (StartTime.equalsIgnoreCase("clr")) {

				if (pathArrayString.equalsIgnoreCase("red")) {
					makeRed();
				} else if (pathArrayString.equalsIgnoreCase("blue")) {
					makeBlue();
				} else if (pathArrayString.equalsIgnoreCase("green")) {
					makeGreen();
				} else if (pathArrayString.equalsIgnoreCase("circle")) {
					makeCircle();
				} else if (pathArrayString.equalsIgnoreCase("erase")) {
					makeErase();
				} else if (pathArrayString.equalsIgnoreCase("path")) {
					makePath();
				}else if (pathArrayString.equalsIgnoreCase("undo")) {
					makeUndo();
				}else if (pathArrayString.equalsIgnoreCase("redo")) {
					makeRedo();
				}


			} else {

				final String pathPoints[] = pathArrayString.split(",");

				final String[] down = pathPoints[0].split("#");

				int delay = calculateDelay(StartTime, EndTime);

				delayLine(delay);

				currentDrawingPath = new DrawingPath();
				currentDrawingPath.paint = currentPaint;
				currentDrawingPath.path = new Path();

				Log.e("up", down[0]);
				Log.e("up", down[1]);
				Log.e("up", down[2]);

				float x = Float.parseFloat(down[1]);
				float y = Float.parseFloat(down[2]);
				currentBrush.mouseDown(currentDrawingPath.path,
						x, y);

				for (int i = 1; i < pathPoints.length - 1; i++) {
					String[] move = pathPoints[i].split("#");
					Log.e("move x", move[1].toString());
					Log.e("move y", move[1].toString());
					x = Float.parseFloat(move[1]);
					y = Float.parseFloat(move[2]);
					currentBrush.mouseMove(
							currentDrawingPath.path, x, y);

				}
				String[] up = pathPoints[pathPoints.length - 1]
						.split("#");
				x = Float.parseFloat(down[1]);
				y = Float.parseFloat(down[2].substring(0,
						down[2].length() - 1));
				Log.e("up x", up[1].toString());
				Log.e("up y", Float.toString(y));
				currentBrush.mouseUp(currentDrawingPath.path,
						x, y);

				drawingSurface
				.myaddDrawingPath(currentDrawingPath);
				drawingSurface.isDrawing = true;
				undoBtn.setEnabled(true);
				redoBtn.setEnabled(false);

				String breakStartT = EndTime;

				int m = r;
				String breakEndT ;

				if(m != msg.length-1)
				{
					do
					{
						m = m+1;
						String path2[] = msg[m].split("<");
						breakEndT = path2[0];


					}while(breakEndT.equalsIgnoreCase("clr") && m < msg.length-1);



					int breakDelay = calculateDelay(breakStartT, breakEndT);


					delayLine(breakDelay);
				}

			}

		}


		//currentBrush.mouseDown(currentDrawingPath.path, motionEvent.getX(), motionEvent.getY());
	}

	public void delayLine(int delay)
	{

		for(int i = 0 ; i<= 10 ; i++)
		{
			final Handler handler = new Handler();

			Timer t = new Timer();
			t.schedule(new TimerTask() {
				public void run() {
					handler.post(new Runnable() {
						public void run() {


						}
					});
				}
			}, delay);
		}
	}



	public int calculateDelay(String startT, String endT)
	{
		String startTArray[] = startT.split(":");
		String endTArray[] = endT.split(":");

		int sHH = Integer.parseInt(startTArray[0]);
		int sMM = Integer.parseInt(startTArray[1]);
		int sSS = Integer.parseInt(startTArray[2]);

		int eHH = Integer.parseInt(endTArray[0]);
		int eMM = Integer.parseInt(endTArray[1]);
		int eSS = Integer.parseInt(endTArray[2]);

		int sT = sHH*60*60 + sMM*60 + sSS;
		int eT = eHH*60*60 + eMM*60 + eSS;

		return (eT-sT)*1000;

	}



	//bind our onTouch on the surfaceView in our ACTIVITY
	public boolean onTouch(View view, MotionEvent motionEvent) {


		return true;
	}

	//get students



	// change the currentPaint's color
	public void onClick(View view){
		switch (view.getId()){

		//Color Red
		case R.id.colorRedBtn:

			currentPaint = new Paint();
			currentPaint.setDither(true);
			currentPaint.setColor(0xFFFF0000);
			currentPaint.setStyle(Paint.Style.STROKE);
			currentPaint.setStrokeJoin(Paint.Join.ROUND);
			currentPaint.setStrokeCap(Paint.Cap.ROUND);
			currentPaint.setStrokeWidth(3);

			break;

		case R.id.btnRec:
		{


			while(drawingSurface.myhasMoreUndo())
				drawingSurface.myundo();

			playBack("record");


		}

		break;
		//Color Blue
		case R.id.colorBlueBtn:
			currentPaint = new Paint();
			currentPaint.setDither(true);
			currentPaint.setColor(0xFF00FF00);
			currentPaint.setStyle(Paint.Style.STROKE);
			currentPaint.setStrokeJoin(Paint.Join.ROUND);
			currentPaint.setStrokeCap(Paint.Cap.ROUND);
			currentPaint.setStrokeWidth(3);

			break;

			//Color Green
		case R.id.colorGreenBtn:
			currentPaint = new Paint();
			currentPaint.setDither(true);
			currentPaint.setColor(0xFF0000FF);
			currentPaint.setStyle(Paint.Style.STROKE);
			currentPaint.setStrokeJoin(Paint.Join.ROUND);
			currentPaint.setStrokeCap(Paint.Cap.ROUND);
			currentPaint.setStrokeWidth(3);

			break;

			//Undo Button
		case R.id.undoBtn:
			drawingSurface.undo();
			if( drawingSurface.hasMoreUndo() == false ){
				undoBtn.setEnabled( false );
			}
			redoBtn.setEnabled( true );
			break;

			//Redo Button
		case R.id.redoBtn:
			drawingSurface.redo();
			if( drawingSurface.hasMoreRedo() == false ){
				redoBtn.setEnabled( false );
			}

			undoBtn.setEnabled( true );
			break;

			//Save Button
		case R.id.saveBtn:
			final Activity currentActivity  = this;
			Handler saveHandler = new Handler(){
				@Override
				public void handleMessage(android.os.Message msg) {
					final AlertDialog alertDialog = new AlertDialog.Builder(currentActivity).create();
					alertDialog.setTitle("Saved successfully.!");
					alertDialog.setMessage("Your drawing had been saved. PIC"+ curentDateandTime +".png");
					alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							return;
						}
					});
					alertDialog.show();
				}
			} ;
			new ExportBitmapToFile(this,saveHandler, drawingSurface.getBitmap()).execute();
			break;

			//Circle paint button
		case R.id.circleBtn:

			currentBrush = new CircleBrush();

			break;

			//Path,Pencil button
		case R.id.pathBtn:
			currentBrush = new PenBrush();

			break;

			//Eraser
		case R.id.eraser:
			currentPaint = new Paint();
			currentPaint.setDither(true);
			currentPaint.setColor(Color.WHITE); //Bcoz background color is also white
			currentPaint.setStyle(Paint.Style.STROKE);
			currentPaint.setStrokeJoin(Paint.Join.ROUND);
			currentPaint.setStrokeCap(Paint.Cap.ROUND);
			currentPaint.setStrokeWidth(20);


			break;

		}
	}


	private class ExportBitmapToFile extends AsyncTask<Intent,Void,Boolean> {
		private Context mContext;
		private Handler mHandler;
		private Bitmap nBitmap;

		public ExportBitmapToFile(Context context,Handler handler,Bitmap bitmap) {
			mContext = context;
			nBitmap = bitmap;
			mHandler = handler;
		}

		@Override
		protected Boolean doInBackground(Intent... arg0) {
			try {
				if (!APP_FILE_PATH.exists()) {
					APP_FILE_PATH.mkdirs();
				}

				/*	mBitmap.compress the compressed bitmap
				 *  would be buffered into that FileOutputStream */
				 final FileOutputStream out = new FileOutputStream(new File(APP_FILE_PATH + "/PIC"+curentDateandTime+".png"));
				 final FileOutputStream outThumb = new FileOutputStream(new File(APP_FILE_PATH + "/PIC"+curentDateandTime+"thumb.png"));

				 nBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

				 final Bitmap resizedBitmap = Bitmap.createScaledBitmap(nBitmap, 275,150, true);

				 resizedBitmap.compress(Bitmap.CompressFormat.PNG, 90, outThumb);

				 out.flush();
				 out.close();

				 outThumb.flush();
				 outThumb.close();
				 return true;
			}catch (Exception e) {
				e.printStackTrace();
			}
			//mHandler.post(completeRunnable);
			return false;
		}


		@Override
		protected void onPostExecute(Boolean bool) {
			super.onPostExecute(bool);
			if ( bool ){
				mHandler.sendEmptyMessage(1);
			}
		}
	}

	/** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Chat");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_action_mail);
			mnu1.setTitle("Chat");
		}


	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{


			Intent intent = new Intent(PlayBackRecord.this, ChatTeach.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			///
			finish();
		}

		return true;

		}
		return false;
	}



}
