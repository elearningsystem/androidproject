package com.almondmendoza.drawings;


import android.graphics.Canvas;

/**
 * ICanvasCommand interface
 * Date: 05/13/2012
 * 
 */
public interface ICanvasCommand {
    public void draw(Canvas canvas);
    public void undo();
}
