package com.almondmendoza.drawings;

import android.graphics.Canvas;
import android.util.Log;

import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

/**
 * CommandManager
 * Date: 05/17/2012
 * 
 */
public class CommandManager {
    private List<DrawingPath> currentStack;
    private List<DrawingPath> redoStack;
    
    private List<DrawingPath> mycurrentStack;
    private List<DrawingPath> myredoStack;


    public  CommandManager(){
    	/*  create 2 List,,, one called currentStack for the current paths,
    	 *  another is redoStack where we put 
    	 *  the undo commands so we could do redo*/
        currentStack = Collections.synchronizedList(new ArrayList<DrawingPath>());
        redoStack = Collections.synchronizedList(new ArrayList<DrawingPath>());
        
        mycurrentStack = Collections.synchronizedList(new ArrayList<DrawingPath>());
        myredoStack = Collections.synchronizedList(new ArrayList<DrawingPath>());
    }

    //when we add a new command, we should clear the redoStack
    public void addCommand(DrawingPath command){
        redoStack.clear();
        currentStack.add(command);
    }

    public void myaddCommand(DrawingPath command){
        myredoStack.clear();
        mycurrentStack.add(command);
    }
    /*	When we do an undo, we pop the last command from
     *  the currentStack and push it to our redoStack.  */
    public void undo (){
        final int length = currentStackLength();
        
        if ( length > 0) {
            final DrawingPath undoCommand = currentStack.get(  length - 1  );
            currentStack.remove( length - 1 );
            undoCommand.undo();
            redoStack.add( undoCommand );
        }
    }

    public void myundo (){
        final int length = mycurrentStackLength();
        
        if ( length > 0) {
            final DrawingPath undoCommand = mycurrentStack.get(  length - 1  );
            mycurrentStack.remove( length - 1 );
            undoCommand.undo();
            myredoStack.add( undoCommand );
        }
    }
    public int currentStackLength(){
        final int length = currentStack.toArray().length;
        return length;
    }
    
    public int mycurrentStackLength(){
        final int length = mycurrentStack.toArray().length;
        return length;
    }


    /*	loop through our currentStack and 
     * do the execute part (.draw) from our command pattern. */
    public void executeAll( Canvas canvas){
        if( currentStack != null ){
            synchronized( currentStack ) {
                final Iterator i = currentStack.iterator();
                Log.d("aaa",currentStack.toArray().length + "");
                while ( i.hasNext() ){
                    final DrawingPath drawingPath = (DrawingPath) i.next();
                    drawingPath.draw( canvas );
                }
            }
        }
    }


    public void myexecuteAll( Canvas canvas){
        if( mycurrentStack != null ){
            synchronized( mycurrentStack ) {
                final Iterator i = mycurrentStack.iterator();
                Log.d("aaa",mycurrentStack.toArray().length + "");
                while ( i.hasNext() ){
                    final DrawingPath drawingPath = (DrawingPath) i.next();
                    drawingPath.draw( canvas );
                }
            }
        }
    }

    public boolean hasMoreRedo(){
        return  redoStack.toArray().length > 0;
    }
    public boolean myhasMoreRedo(){
        return  myredoStack.toArray().length > 0;
    }

    public boolean hasMoreUndo(){
        return  currentStack.toArray().length > 0;
    }

    public boolean myhasMoreUndo(){
        return  mycurrentStack.toArray().length > 0;
    }
    public void redo(){
        final int length = redoStack.toArray().length;
        if ( length > 0) {
            final DrawingPath redoCommand = redoStack.get(  length - 1  );
            redoStack.remove( length - 1 );
            currentStack.add( redoCommand );
        }
    }
    
    public void myredo(){
        final int length = myredoStack.toArray().length;
        if ( length > 0) {
            final DrawingPath redoCommand = myredoStack.get(  length - 1  );
            myredoStack.remove( length - 1 );
            mycurrentStack.add( redoCommand );
        }
    }
}
