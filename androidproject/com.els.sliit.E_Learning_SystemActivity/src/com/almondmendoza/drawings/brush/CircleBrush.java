package com.almondmendoza.drawings.brush;

import android.graphics.Path;

/**
 * CircleBrush
 * Date: 05/17/2012
 * 
 */
public class CircleBrush extends Brush{

    @Override
    public void mouseMove(Path path, float x, float y) {
        path.addCircle(x,y,5,Path.Direction.CW);
       
    }

}
