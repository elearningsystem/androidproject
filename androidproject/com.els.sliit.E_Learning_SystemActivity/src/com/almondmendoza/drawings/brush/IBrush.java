package com.almondmendoza.drawings.brush;

import android.graphics.Path;

/**
 * IBrush
 * Date: 05/17/2012
 * 
 */
public interface IBrush {
    public void mouseDown( Path path, float x, float y);
    public void mouseMove( Path path, float x, float y);
    public void mouseUp( Path path, float x, float y);
}
