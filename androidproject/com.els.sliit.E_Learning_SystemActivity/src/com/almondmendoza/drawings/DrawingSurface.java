package com.almondmendoza.drawings;

import android.R.color;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.els.sliit.GlobalClass;

/**
 * DrawingSurface
 * Date: 05/17/2012
 * 
 */
public class DrawingSurface extends SurfaceView implements SurfaceHolder.Callback {
    private Boolean _run;
    protected DrawThread thread;
    private Bitmap mBitmap;
    
    //tell if we are drawing or not.
    public boolean isDrawing = true;

    private CommandManager commandManager;

    public DrawingSurface(Context context, AttributeSet attrs) {
        super(context, attrs);

        getHolder().addCallback(this);


        commandManager = new CommandManager();
        thread = new DrawThread(getHolder());
    }


    
    /*draw the preview path and pass our 
     * complete handler to stop 
     * our app from drawing over and over*/
    class DrawThread extends  Thread{
        private SurfaceHolder mSurfaceHolder;

        public DrawThread(SurfaceHolder surfaceHolder){
            mSurfaceHolder = surfaceHolder;

        }

        public void setRunning(boolean run) {
            _run = run;
        }


        @Override
        public void run() {
            Canvas canvas = null;
            while (_run){
                if(isDrawing == true){
                    try{
                        canvas = mSurfaceHolder.lockCanvas(null);
                        if(mBitmap == null){
                            mBitmap =  Bitmap.createBitmap (1, 1, Bitmap.Config.ARGB_8888);
                        }
                        final Canvas c = new Canvas (mBitmap);

                        
                        //create a canvas with bitmap to draw
                        c.drawColor(0, PorterDuff.Mode.CLEAR);
                        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                        
                        canvas.drawColor(0xFFFFFFFF); // making background color to white
                        //commandManager.executeAll(c);

                        if(GlobalClass.myScreen){
                        	commandManager.myexecuteAll(c);
                        }
                        else{
                        	commandManager.executeAll(c);
                        }
                        //put our bitmap to our original canvas (the surfaceView's canvas) so that we can see our drawings
                        canvas.drawBitmap (mBitmap, 0,  0,null);
                    } finally {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    isDrawing = false;
                }

            }

        }
    }


    public void addDrawingPath (DrawingPath drawingPath){
        commandManager.addCommand(drawingPath);
    }
    
    public void myaddDrawingPath (DrawingPath drawingPath){
        commandManager.myaddCommand(drawingPath);
    }

    public boolean hasMoreRedo(){
        return commandManager.hasMoreRedo();
    }
    public boolean myhasMoreRedo(){
        return commandManager.myhasMoreRedo();
    }


    public void redo(){
        isDrawing = true;
        commandManager.redo();


    }
    public void myredo(){
        isDrawing = true;
        commandManager.myredo();


    }

    public void undo(){
        isDrawing = true;
        commandManager.undo();
    }

    public void myundo(){
        isDrawing = true;
        commandManager.myundo();
    }
    public boolean hasMoreUndo(){
        return commandManager.hasMoreUndo();
    }

    public boolean myhasMoreUndo(){
        return commandManager.myhasMoreUndo();
    }

    public Bitmap getBitmap(){
        return mBitmap;
    }


    /*recreate our bitmap when we change the orientation*/
    public void surfaceChanged(SurfaceHolder holder, int format, int width,  int height) {
        // TODO Auto-generated method stub
        mBitmap =  Bitmap.createBitmap (width, height, Bitmap.Config.ARGB_8888);;
    }


    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        thread.setRunning(true);
        thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
                // we will try it again and again...
            }
        }
    }

}
