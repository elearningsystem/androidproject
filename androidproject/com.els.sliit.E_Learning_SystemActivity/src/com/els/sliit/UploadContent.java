/*
* UploadContent
*
* Version 1.0
*
*	This class will handle file upload activities
*/
package com.els.sliit;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.TokenPair;
import com.dropbox.client2.session.Session.AccessType;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class UploadContent extends Activity {

	//Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb = null;
	String privateCatogory = "";
	String filePath;
	
	private static final String TAG = "Elearning";
	final static private String APP_KEY = "pka0rwvu6t8e0rs";
    final static private String APP_SECRET = "htenug3ozbiwm8d";
    
    final static private String ACCOUNT_PREFS_NAME = "prefs";
    final static private String ACCESS_KEY_NAME = "ACCESS_KEY";
    final static private String ACCESS_SECRET_NAME = "ACCESS_SECRET";

    // If you'd like to change the access type to the full Dropbox instead of
    // an app folder, change this value.
    final static private AccessType ACCESS_TYPE = AccessType.DROPBOX;
    
 // In the class declaration section:
    private DropboxAPI<AndroidAuthSession> mDBApi;

    private final String DISK_DIR = "/Public/";

    final static private int NEW_PICTURE = 1;
    private String mFileName;
    

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_fragment);
		checkAppKeySetup();
		
		//Allow network access from the main thread
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		// And later in some initialization function:
	    AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
	    AndroidAuthSession session = buildSession();
        	    
	    mDBApi = new DropboxAPI<AndroidAuthSession>(session);
	    if(mDBApi.getSession().isLinked()==false)
	    mDBApi.getSession().startAuthentication(UploadContent.this);

		//Set window title
		ActionBar acBar = getActionBar();
		acBar.setTitle("Share Content");

		// load history values
		EditText txtFilePath = (EditText) findViewById(R.id.txtFilePath);
		txtFilePath.setText(GlobalClass.getFilePath());

		EditText txtShareMessage = (EditText) findViewById(R.id.txtMessage);
		txtShareMessage.setText(GlobalClass.getShareMessage());

		//When message changed the text is saved to the golbalClass
		txtShareMessage.addTextChangedListener(new TextWatcher() {

			
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				GlobalClass.setShareMessage(s.toString());
			}
		});

		//Browse button click event
		Button btnBrowse = (Button) findViewById(R.id.btnBrowse);

		btnBrowse.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				// Load DirectoryBrowser
				startActivity(new Intent(UploadContent.this,
						DirectoryBrowser.class));
				finish();//
				

			}
		});

		// Catogory selector alert box for publicSharing

		final CharSequence[] items = { "General", "Programming", "Assignments",
				"Tutorials" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick a Catogory");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {

				Toast.makeText(getApplicationContext(), items[item],
						Toast.LENGTH_SHORT).show();
				String filePath = GlobalClass.getFilePath();
				doFileUpload(filePath);
				updateContentTable(items[item].toString(),"public");
				
				clearFields();

			}
		});

		final AlertDialog pickCatogoryAlert = builder.create();

		// Catogory selector alert box for privateSharing
		AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
		builder2.setTitle("Pick a Catogory");
		builder2.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {

				Toast.makeText(getApplicationContext(), items[item],
						Toast.LENGTH_SHORT).show();

				privateSharingDialog(items[item].toString());

			}
		});
		final AlertDialog pickCatogoryAlertPrivate = builder2.create();

		//Public Share button click event
		Button btnShare = (Button) findViewById(R.id.btnShare);

		btnShare.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EditText txtMessage = (EditText) findViewById(R.id.txtMessage);
				EditText txtFilePath = (EditText) findViewById(R.id.txtFilePath);

				String message = txtMessage.getText().toString();
				String filePath = txtFilePath.getText().toString();

				if (!filePath.isEmpty())
					if (!message.isEmpty()) {
						pickCatogoryAlert.show();
					} else {
						Toast.makeText(getApplicationContext(),
								"Type a message!!", Toast.LENGTH_SHORT).show();
					}
				else
					Toast.makeText(getApplicationContext(),
							"Browse a File to upload!", Toast.LENGTH_SHORT)
							.show();

			}
		});

		//Private Share button click event
		Button btnPrivateShare = (Button) findViewById(R.id.btnSharePrivate);

		btnPrivateShare.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				EditText txtMessage = (EditText) findViewById(R.id.txtMessage);
				EditText txtFilePath = (EditText) findViewById(R.id.txtFilePath);

				String message = txtMessage.getText().toString();
				String filePath = txtFilePath.getText().toString();

				if (!filePath.isEmpty())
					if (!message.isEmpty()) {

						pickCatogoryAlertPrivate.show();

					} else {
						Toast.makeText(getApplicationContext(),
								"Type a message!!", Toast.LENGTH_SHORT).show();
					}
				else
					Toast.makeText(getApplicationContext(),
							"Browse a File to upload!", Toast.LENGTH_SHORT)
							.show();

			}
		});

	}

	/** Private Sharing - Lets user to select people and share the content only
	for them. */
	public void privateSharingDialog(String catogory) {
		
		final String pCatogory = catogory;
		
		// Connect with the web server 
		WebConnectivity webConn = new WebConnectivity();

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("userName", GlobalClass
				.getUsername()));
		
		// Send request to the web server
		InputStream iStream = webConn.httpPost("eLearningWebServices/Content/GetUserNames.php",
				nameValuePairs);
		String stringResponse = webConn.convertResponse(iStream);

		String value = "";

		CharSequence[] nameList = null;

		int size = 1;
		
		try {

			jArray = new JSONArray(stringResponse);
			JSONObject json_data = null;

			size = jArray.length();

			nameList = new CharSequence[size];

			for (int i = 0; i < size; i++) {
				json_data = jArray.getJSONObject(i);
				value = json_data.getString("username");

				nameList[i] = value;
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}

		// Select people alert box
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
		alt_bld.setIcon(R.drawable.ic_launcher);
		alt_bld.setTitle("Select People");

		alt_bld.setMultiChoiceItems(nameList, new boolean[size],
				new DialogInterface.OnMultiChoiceClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton, boolean isChecked) {

					}
				});

		alt_bld.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
			public void onClick(DialogInterface dialog, int which) {

				boolean isSeleted = false;

				ListView list = ((AlertDialog) dialog).getListView();

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < list.getCount(); i++) {
					boolean checked = list.isItemChecked(i);

					if (checked) {
						if (sb.length() > 0)
							sb.append(",");
						sb.append(list.getItemAtPosition(i));
						isSeleted = true;

					}
				}

				if (isSeleted) {
					
					//Upload file to server
					String storageFilePath = GlobalClass.getFilePath();
					doFileUpload(storageFilePath);
					
					//Update Content Table
					updateContentTable(pCatogory,"private");

					//Insert in to Private Content Table
					WebConnectivity webConn2 = new WebConnectivity();

					String filePath = GlobalClass.getFilePath();
					String fileName = filePath.substring(
							filePath.lastIndexOf('/') + 1, filePath.length());

					ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("userName",
							GlobalClass.getUsername()));
					nameValuePairs.add(new BasicNameValuePair("fileName",
							fileName));
					nameValuePairs.add(new BasicNameValuePair("uid",
							GlobalClass.getUsername() + "," + sb.toString()));

					InputStream iStream = webConn2.httpPost(
							"eLearningWebServices/Content/InsertPrivateContent.php", nameValuePairs);
					String stringResponse = webConn2.convertResponse(iStream);

					String status = null;

					try {

						jArray = new JSONArray(stringResponse);
						JSONObject json_data = null;
						for (int i = 0; i < jArray.length(); i++) {
							json_data = jArray.getJSONObject(i);
							status = json_data.getString("id");

						}
						if (!(status.equals("-1")))
							Toast.makeText(getBaseContext(),
									"Shared only among selected people",
									Toast.LENGTH_LONG).show();
						else {
							Toast.makeText(getBaseContext(),
									"Error in Connection!", Toast.LENGTH_LONG)
									.show();
						}

					} catch (JSONException e1) {
						Toast.makeText(getBaseContext(), "Error",
								Toast.LENGTH_LONG).show();
						Log.e("log_tag", e1.toString());

					} catch (ParseException e1) {
						e1.printStackTrace();

					}

					clearFields();
				} else {
					Toast.makeText(getBaseContext(),
							"Select atleast one person!", Toast.LENGTH_LONG)
							.show();
				}

			}
		});
		alt_bld.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {

					}
				});

		AlertDialog alert = alt_bld.create();
		alert.show();

	}

	/** Update content_table */
	public void updateContentTable(String catogory, String visibility ) {

		EditText txtMessage = (EditText) findViewById(R.id.txtMessage);
		EditText txtFilePath = (EditText) findViewById(R.id.txtFilePath);

		String message = txtMessage.getText().toString();
	    filePath = txtFilePath.getText().toString();

		String fileName = filePath.substring(filePath.lastIndexOf('/') + 1,
				filePath.length());
		String fileExtension = fileName.substring(
				fileName.lastIndexOf('.') + 1, fileName.length());
		String userName = GlobalClass.getUsername();

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("message", message));
		nameValuePairs.add(new BasicNameValuePair("filePath", fileName));
		nameValuePairs.add(new BasicNameValuePair("catogory", catogory));
		nameValuePairs.add(new BasicNameValuePair("userName", userName));
		nameValuePairs.add(new BasicNameValuePair("type", fileExtension));
		nameValuePairs.add(new BasicNameValuePair("visibility", visibility));

		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://10.0.2.2:1000/eLearningWebServices/Content/UpdateContent.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			// Log.e("log_tag", result.toString());
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		// paring data
		String status = null;

		try {

			jArray = new JSONArray(result);
			JSONObject json_data = null;
			for (int i = 0; i < jArray.length(); i++) {
				json_data = jArray.getJSONObject(i);
				status = json_data.getString("status");

			}
			if (!(status.equals("Error")))
			{}
				//Toast.makeText(getBaseContext(), "Content is Shared",
						//Toast.LENGTH_LONG).show();
			else {
				Toast.makeText(getBaseContext(),
						"Can't share the content, Try later", Toast.LENGTH_LONG)
						.show();
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "Error Connecting to Database",
					Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}
	}

	/** Upload given file to the server */
	private void doFileUpload(String filePath) {
		
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String existingFileName = filePath;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		String responseFromServer = "";

		String userName = GlobalClass.getUsername();

		String urlString = "http://10.0.2.2:1000/eLearningWebServices/Content/Upload.php?userName=" + userName;


		try {
			// ------------------ CLIENT REQUEST
			FileInputStream fileInputStream = new FileInputStream(new File(
					existingFileName));
			// open a URL connection to the Servlet
			URL url = new URL(urlString);
			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();
			// Allow Inputs
			conn.setDoInput(true);
			// Allow Outputs
			conn.setDoOutput(true);
			// Don't use a cached copy.
			conn.setUseCaches(false);

			// Use a post method.
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			dos = new DataOutputStream(conn.getOutputStream());
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
					+ existingFileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);
			
			// create a buffer of maximum size
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			
			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			
			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// close streams
			Log.e("Debug", "File is written");

			//Toast toast = Toast.makeText(getBaseContext(),
					//"File is Uploaded Sucessfully", Toast.LENGTH_LONG);
			//toast.show();

			fileInputStream.close();
			dos.flush();
			dos.close();
		} catch (MalformedURLException ex) {
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		} catch (IOException ioe) {
			Log.e("Debug", "error: " + ioe.getMessage(), ioe);
		}
		// ------------------ read the SERVER RESPONSE
		try {
			inStream = new DataInputStream(conn.getInputStream());
			String str;

			while ((str = inStream.readLine()) != null) {
				Log.e("Debug", "Server Response " + str);
			}
			inStream.close();

		} catch (IOException ioex) {
			Log.e("Debug", "error: " + ioex.getMessage(), ioex);
		}
		//creating a file object
		File Docfile = new File(filePath);
		//creating an object of UploadDocument class and passing values to its constructor
		UploadDocument upload = new UploadDocument(this, mDBApi, DISK_DIR, Docfile);
        upload.execute();
	}

	/** Clears the text boxes and path and message variables */
	private void clearFields() {
		GlobalClass.setFilePath("");
		GlobalClass.setShareMessage("");

		EditText txtFilePath = (EditText) findViewById(R.id.txtFilePath);
		txtFilePath.setText(GlobalClass.getFilePath());

		EditText txtShareMessage = (EditText) findViewById(R.id.txtMessage);
		txtShareMessage.setText(GlobalClass.getShareMessage());
	}

	/** Add Share menu item to action bar */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu) {
		MenuItem mnu1 = menu.add(0, 0, 0, "Share");
		{
			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
					| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Share");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0: {
			startActivity(new Intent(UploadContent.this, ShareView.class));
			this.finish();//
		}
			return true;

		}
		return false;
	}
	
	
	 protected void onSaveInstanceState(Bundle outState) {
	        outState.putString("mCameraFileName", mFileName);
	        super.onSaveInstanceState(outState);
	    }

	    @Override
	    protected void onResume() {
	        super.onResume();
	        AndroidAuthSession session = mDBApi.getSession();

	        // The next part must be inserted in the onResume() method of the
	        // activity from which session.startAuthentication() was called, so
	        // that Dropbox authentication completes properly.
	        if (session.authenticationSuccessful()) {
	            try {
	                // Mandatory call to complete the auth
	                session.finishAuthentication();

	                // Store it locally in our app for later use
	                TokenPair tokens = session.getAccessTokenPair();
	                storeKeys(tokens.key, tokens.secret);
	                
	            } catch (IllegalStateException e) {
	                showToast("Couldn't authenticate with Dropbox:" + e.getLocalizedMessage());
	                Log.i(TAG, "Error authenticating", e);
	            }
	        }
	    }

	    // This is what gets called on finishing a media piece to import
	    
	    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	        if (requestCode == NEW_PICTURE) {
	            // return from file upload
	            if (resultCode == Activity.RESULT_OK) {
	                Uri uri = null;
	                if (data != null) {
	                    uri = data.getData();
	                }
	                if (uri == null && mFileName != null) {
	                    uri = Uri.fromFile(new File(mFileName));
	                }
	                File file = new File(mFileName);

	                if (uri != null) {
	                    //UploadPicture upload = new UploadPicture(this, mDBApi, DISK_DIR, file);
	                    //upload.execute();
	                }
	            } else {
	                Log.w(TAG, "Unknown Activity Result from mediaImport: "
	                        + resultCode);
	            }
	        }
	    }


	    /**
	     * Convenience function to change UI state based on being logged in
	     */
	    

	    private void checkAppKeySetup() {
	        // Check to make sure that we have a valid app key
	        if (APP_KEY.startsWith("CHANGE") ||
	                APP_SECRET.startsWith("CHANGE")) {
	            showToast("You must apply for an app key and secret from developers.dropbox.com, and add them to the DBRoulette ap before trying it.");
	            finish();
	            return;
	        }

	        // Check if the app has set up its manifest properly.
	        Intent testIntent = new Intent(Intent.ACTION_VIEW);
	        String scheme = "db-" + APP_KEY;
	        String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
	        testIntent.setData(Uri.parse(uri));
	        PackageManager pm = getPackageManager();
	        if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
	            showToast("URL scheme in your app's " +
	                    "manifest is not set up correctly. You should have a " +
	                    "com.dropbox.client2.android.AuthActivity with the " +
	                    "scheme: " + scheme);
	            finish();
	        }
	    }

	    private void showToast(String msg) {
	        Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
	        error.show();
	    }

	    /**
	     * Shows keeping the access keys returned from Trusted Authenticator in a local
	     * store, rather than storing user name & password, and re-authenticating each
	     * time (which is not to be done, ever).
	     *
	     * @return Array of [access_key, access_secret], or null if none stored
	     */
	    private String[] getKeys() {
	        SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	        String key = prefs.getString(ACCESS_KEY_NAME, null);
	        String secret = prefs.getString(ACCESS_SECRET_NAME, null);
	        if (key != null && secret != null) {
	        	String[] ret = new String[2];
	        	ret[0] = key;
	        	ret[1] = secret;
	        	return ret;
	        } else {
	        	return null;
	        }
	    }

	    /**
	     * Shows keeping the access keys returned from Trusted Authenticator in a local
	     * store, rather than storing user name & password, and re-authenticating each
	     * time (which is not to be done, ever).
	     */
	    private void storeKeys(String key, String secret) {
	        // Save the access key for later
	        SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	        Editor edit = prefs.edit();
	        edit.putString(ACCESS_KEY_NAME, key);
	        edit.putString(ACCESS_SECRET_NAME, secret);
	        edit.commit();
	    }

	    private void clearKeys() {
	        SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	        Editor edit = prefs.edit();
	        edit.clear();
	        edit.commit();
	    }

	    private AndroidAuthSession buildSession() {
	        AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);
	        AndroidAuthSession session;

	        String[] stored = getKeys();
	        if (stored != null) {
	            AccessTokenPair accessToken = new AccessTokenPair(stored[0], stored[1]);
	            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE, accessToken);
	        } else {
	            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE);
	        }

	        return session;
	    }
	    
	    
}
