/*
 * Group
 *
 * Version 1.0
 *
 * Handle groups and their categories 
 */

package com.els.sliit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.R.anim;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.content.ClipData.Item;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class GroupActivity extends Activity {

	GridView gridView;
	private JSONArray contacts;
	private static String url = "http://10.0.2.2:1000/eLearningWebServices/Group/GetUser.php";

	// JSON Node names
	private static final String TAG_CONTACTS = "Unames";

	private static final String TAG_NAME = "name";
	private static final String USER_NAME = "username";


	private String[] Name ;
	private String[] Usernames ;
	private LinearLayout layoutDropArea;
	private ListAdapter adapter;
	private InputStream is;
	private StringBuilder sb;
	private String result;
	private JSONArray jArray;
	private String[] SearchUsernames;
	private String[] SearchName;
	private ArrayAdapter<String> catAdapter;
	String addedusername;
	private String addedName;
	String nameDroped =null;
	private String CatName;
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Set xml layout of the group
		setContentView(R.layout.group);

		// Enable main thread to access network
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// Change the action bar title
		ActionBar acBar = getActionBar();
		acBar.setTitle("Manage Groups");

		gridView = (GridView) findViewById(R.id.gridView1);

		EditText searchtxt = (EditText)findViewById(R.id.editText1);

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = jParser.getJSONFromUrl(url);

		adapter = new ImageAdapter(this, Name,Usernames);
		try {
			// Getting Array of Contacts
			contacts = json.getJSONArray(TAG_CONTACTS);

			Name = new String[contacts.length()];
			Usernames = new String[contacts.length()];
			// looping through All Contacts
			for(int i = 0; i < contacts.length(); i++){
				JSONObject c = contacts.getJSONObject(i);

				// Storing each json item in variable

				String val1 = c.getString(USER_NAME);
				String val2 = c.getString(TAG_NAME);

				Usernames[i]=val1;
				Name[i]=val2;
			}
			gridView.setAdapter(new ImageAdapter(this, Name,Usernames));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Button refresh click event
		Button refresh = (Button) findViewById(R.id.btnRefresh);
		refresh.setOnClickListener(new OnClickListener() {

	
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Refresh();
				TextView txtCat = (TextView) findViewById(R.id.txtCatName);
				txtCat.setText("All users");
			}
		});

		//set category list view
		try {
			setCategoryListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "No groups found..! You can create your own group categories.",
					Toast.LENGTH_LONG).show();
		}

		//change the gridview data according to search text
		searchtxt.addTextChangedListener(new TextWatcher()
		{


	
			public void onTextChanged( CharSequence arg0, int arg1, int arg2, int arg3)
			{
				// TODO Auto-generated method stub


			}

		
			public void beforeTextChanged( CharSequence arg0, int arg1, int arg2, int arg3)
			{
				// TODO Auto-generated method stub

			}

		
			public void afterTextChanged( Editable arg0)
			{
				// TODO Auto-generated method stub
				//Refresh();

				Name = new String[contacts.length()];
				Usernames = new String[contacts.length()];
				//((Filterable) Gridview1Activity.this.adapter).getFilter().filter(arg0);
				TextView txtCat = (TextView) findViewById(R.id.txtCatName);
				txtCat.setText("Searched result");
				EditText editsearch = (EditText) findViewById(R.id.editText1);
				String search = editsearch.getText().toString();
				Search(search);

			}
		});

		//Create drag event
		GridView gView = (GridView) findViewById(R.id.gridView1);

		gView.setOnItemLongClickListener(new OnItemLongClickListener() {

		
			public boolean onItemLongClick(AdapterView<?> arg0, View v, int position, long arg3) {

				final String title = Name[position].toString();//(String) ((TextView) v).getText();
				addedName = Name[position].toString();
				addedusername = Usernames[position].toString();
				final String textData = title + ":" + position;
				ClipData data = ClipData.newPlainText(title, textData);
				v.startDrag(data, new MyDragShadowBuilder1(v), null, 0);
				return true;
			}
		});

		//add to group drag event
		setupDragDropAddtoGroup();
		
		//create new group and add drag event
		setupDragDropNewGroup();
		
		//remove from a category drag event 
		setupDragDropRemove();

		ListView lvCat  = (ListView) findViewById(R.id.listView1);
		lvCat.setOnItemClickListener(new OnItemClickListener() {


			
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String catName = ((TextView)arg1.findViewById(R.id.categorytext)).getText().toString();
				TextView txtCat = (TextView) findViewById(R.id.txtCatName);
				txtCat.setText(catName);
				CatName = txtCat.getText().toString();
				fillGridView(catName);
			}
		});
	}

	private void setupDragDropAddtoGroup() {

		
		ImageView imageDD = (ImageView) findViewById(R.id.imageView1);
		//TextView tv = (TextView) findViewById(R.id.txtUserName);


		// Set the Drag Listener to the drop area.
		imageDD.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				System.out.println(v.getClass().getName());
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_ENTERED:
					v.setBackgroundColor(Color.GRAY);
					break;

				case DragEvent.ACTION_DRAG_EXITED:
					v.setBackgroundColor(Color.TRANSPARENT);
					break;

				case DragEvent.ACTION_DRAG_STARTED:
					return processDragStarted(event);

				case DragEvent.ACTION_DROP:
					v.setBackgroundColor(Color.TRANSPARENT);
					return processDrop(event);
				}
				return false;
			}
		});

	}
	public void setupDragDropRemove()
	{

		ImageView imageRemove = (ImageView) findViewById(R.id.imgRemove);

		// Set the Drag Listener to the drop area.
		imageRemove.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				System.out.println(v.getClass().getName());
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_ENTERED:
					v.setBackgroundColor(Color.GRAY);
					break;

				case DragEvent.ACTION_DRAG_EXITED:
					v.setBackgroundColor(Color.TRANSPARENT);
					break;

				case DragEvent.ACTION_DRAG_STARTED:
					return processDragStarted(event);

				case DragEvent.ACTION_DROP:
					v.setBackgroundColor(Color.TRANSPARENT);
					return processDropRemove(event);
				}
				return false;
			}
		});

	}

	private boolean processDropRemove(DragEvent event) {
		ClipData data = event.getClipData();
		if (data != null) {
			if (data.getItemCount() > 0) {
				Item item = data.getItemAt(0);
				String textData = (String) item.getText();
				String[] parts = textData.split(":");
				int index = Integer.parseInt(parts[1]);
				String listItem = parts[0];
				updateViewsAfterDropCompleteRemove(listItem, index);
				Log.v("test",listItem);
				return true;
			}
		}
		return false;
	}

	private void updateViewsAfterDropCompleteRemove(String listItem, int index) {

		TextView uname = (TextView) findViewById(R.id.txtUserName);
		ImageView img = (ImageView) findViewById(R.id.imageProf);
		img.setImageBitmap(downloadFile("http://10.0.2.2:1000/eLearningWebServices/Profile/profPics/"+addedusername+".jpg"));
		uname.setText(listItem.toString());
		nameDroped = listItem.toString();

		RemoveFromGroup();



	}

	public void RemoveFromGroup(){
		AlertDialog.Builder alert = new AlertDialog.Builder(GroupActivity.this);

		alert.setTitle("Remove From Group");
		alert.setMessage("Do you really want to remove "+nameDroped+ " from this category.?");

		// Set an EditText view to get user input 
		alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				RemoveFromCat(CatName);

				if(status.equals("Success")){
					Toast.makeText(getApplicationContext(), "Successfully Removed from " + CatName+" category",
							Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(getApplicationContext(),"Error..! No such person in this group.",
							Toast.LENGTH_LONG).show();
				}
				setCategoryListView();
			}
		});
		alert.show();
	}
	public void setupDragDropNewGroup()
	{

		ImageView imageNewGrp = (ImageView) findViewById(R.id.imgNewGrp);

		// Set the Drag Listener to the drop area.
		imageNewGrp.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				System.out.println(v.getClass().getName());
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_ENTERED:
					v.setBackgroundColor(Color.GRAY);
					break;

				case DragEvent.ACTION_DRAG_EXITED:
					v.setBackgroundColor(Color.TRANSPARENT);
					break;

				case DragEvent.ACTION_DRAG_STARTED:
					return processDragStarted(event);

				case DragEvent.ACTION_DROP:
					v.setBackgroundColor(Color.TRANSPARENT);
					return processDropNewGrp(event);
				}
				return false;
			}
		});
	}
	/**
	 * Process the drop event
	 * 
	 * @param event
	 * @return
	 */
	private boolean processDrop(DragEvent event) {
		ClipData data = event.getClipData();
		if (data != null) {
			if (data.getItemCount() > 0) {
				Item item = data.getItemAt(0);
				String textData = (String) item.getText();
				String[] parts = textData.split(":");
				int index = Integer.parseInt(parts[1]);
				String listItem = parts[0];
				updateViewsAfterDropComplete(listItem, index);
				Log.v("test",listItem);
				return true;
			}
		}
		return false;
	}

	/**
	 * Update the Views after the drag operation is complete
	 * 
	 * @param listItem
	 * @param index
	 */

	private void updateViewsAfterDropComplete(String listItem, int index) {

		TextView uname = (TextView) findViewById(R.id.txtUserName);
		ImageView img = (ImageView) findViewById(R.id.imageProf);
		img.setImageBitmap(downloadFile("http://10.0.2.2:1000/eLearningWebServices/Profile/profPics/"+addedusername+".jpg"));
		uname.setText(addedName);
		nameDroped = listItem.toString();

		pickAGroup();

	}

	private void pickAGroup() {

		try {
			selectGroups();

			if(!(categories.length==0)){
				// Catogory selector alert box for privateSharing
				AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
				builder2.setTitle("Pick a Catogory");
				builder2.setItems(categories, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {

						addTocategory(categories[item].toString());

						//privateSharingDialog(items[item].toString());
						if(status.equals("Success")){
							Toast.makeText(getApplicationContext(), "Successfully added to " + categories[item]+" category",
									Toast.LENGTH_LONG).show();
						}
						else {
							Toast.makeText(getApplicationContext(),"Error..! This person is already in this category..",
									Toast.LENGTH_LONG).show();
						}

					}
				});
				builder2.show();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),"Error..! This person is already in this category..",
					Toast.LENGTH_LONG).show();
		}

	}

	private boolean processDropNewGrp(DragEvent event) {
		ClipData data = event.getClipData();
		if (data != null) {
			if (data.getItemCount() > 0) {
				Item item = data.getItemAt(0);
				String textData = (String) item.getText();
				String[] parts = textData.split(":");
				int index = Integer.parseInt(parts[1]);
				String listItem = parts[0];
				updateViewsAfterDropCompleteNewGrp(listItem, index);
				Log.v("test",listItem);
				return true;
			}
		}
		return false;
	}

	private void updateViewsAfterDropCompleteNewGrp(String listItem, int index) {

		TextView uname = (TextView) findViewById(R.id.txtUserName);
		ImageView img = (ImageView) findViewById(R.id.imageProf);
		img.setImageBitmap(downloadFile("http://10.0.2.2:1000/eLearningWebServices/Profile/profPics/"+addedusername+".jpg"));
		uname.setText(listItem.toString());
		nameDroped = listItem.toString();

		AddaNewGroup();

	}

	public void AddaNewGroup(){
		AlertDialog.Builder alert = new AlertDialog.Builder(GroupActivity.this);

		alert.setTitle("Create a new Category");
		alert.setMessage("Please Enter Category Name");

		// Set an EditText view to get user input 
		final EditText input = new EditText(GroupActivity.this);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
		alert.setView(input);

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		//if user clicks OK button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String catName = input.getText().toString();
				addTocategory(catName);

				if(status.equals("Success")){
					Toast.makeText(getApplicationContext(), "Successfully added to " + catName+" category",
							Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(getApplicationContext(),"Error..! This person is already in this category..",
							Toast.LENGTH_LONG).show();
				}
				setCategoryListView();
			}
		});
		alert.show();
	}


	/**
	 * Check if this is the drag operation you want. There might be other
	 * clients that would be generating the drag event. Here, we check the mime
	 * type of the data
	 * 
	 * @param event
	 * @return
	 */

	private boolean processDragStarted(DragEvent event) {
		ClipDescription clipDesc = event.getClipDescription();
		if (clipDesc != null) {
			return clipDesc.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);
		}
		return false;
	}

	private void Search(String txtSearch){
		//EditText editsearch = (EditText) findViewById(R.id.editText1);
		String search = txtSearch;

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		//http post
		nameValuePairs.add(new BasicNameValuePair("Name",search));
		nameValuePairs.add(new BasicNameValuePair("userNameMe",GlobalClass.getUsername()));
		//nameValuePairs.add(new BasicNameValuePair("password",password));

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Group/search.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}
		catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//paring data
		//String status="";

		try{
			//JSONObject jobj = new JSONObject(result);
			//jArray = jobj.getJSONArray("uNames");
			jArray = new JSONArray(result);
			JSONObject json_data=null;
			SearchName = new String[jArray.length()];
			Name = new String[jArray.length()];

			for(int i=0;i<jArray.length();i++)
			{
				json_data = jArray.getJSONObject(i);
				//status=json_data.getString("status");
				String val1 = json_data.getString(USER_NAME);
				String val2 = json_data.getString(TAG_NAME);

				Usernames[i]=val1;
				Name[i]=val2;

			}
			gridView.setAdapter(new ImageAdapter(this, Name,Usernames));
		}
		catch(JSONException e1)
		{
			Log.e("log_tag", e1.toString());
		} 

	}

	String[] categories ;
	private Bitmap bmImg;
	private String status;


	//select categories to fill the category list
	public void selectGroups()
	{

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("userName", GlobalClass.getUsername()));
		//http post

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Group/GetCategory.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}
		catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//paring data

		try{

			jArray = new JSONArray(result);
			JSONObject json_data=null;
			categories = new String[jArray.length()];

			for(int i=0;i<jArray.length();i++)
			{
				json_data = jArray.getJSONObject(i);
				//status=json_data.getString("status");
				String val1 = json_data.getString("category");

				categories[i]=val1;
			}

		}
		catch(JSONException e1)
		{
			Log.e("log_tag", e1.toString());
		} 


	}
	
	//download profile picture from the server according to their usernames
	public Bitmap downloadFile(String fileUrl){
		URL myFileUrl =null; 
		try {
			myFileUrl= new URL(fileUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn= (HttpURLConnection)myFileUrl.openConnection();
			conn.setDoInput(true);
			conn.connect();
			int length = conn.getContentLength();
			int[] bitmapData =new int[length];
			byte[] bitmapData2 =new byte[length];
			InputStream is = conn.getInputStream();

			bmImg = BitmapFactory.decodeStream(is);

		} catch (IOException e) {

			Log.e("log_tag", e.getMessage().toString());

		}
		return bmImg;


	}

	//add a user to a selected category
	public void addTocategory(String cat){

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", GlobalClass.getUsername()));
		nameValuePairs.add(new BasicNameValuePair("friend_un", addedusername));
		nameValuePairs.add(new BasicNameValuePair("friend_name", addedName));
		nameValuePairs.add(new BasicNameValuePair("category", cat));

		//http post

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Group/AddToCategory.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}
		catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//paring data

		try{

			jArray = new JSONArray(result);
			JSONObject json_data=null;


			for(int i=0;i<jArray.length();i++)
			{
				json_data = jArray.getJSONObject(i);
				//status=json_data.getString("status");
				status  = json_data.getString("status");

			}

		}
		catch(JSONException e1)
		{
			Log.e("log_tag", e1.toString());
		} 

	}

	//remove a user from particular group
	public void RemoveFromCat(String cat){

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", GlobalClass.getUsername()));
		nameValuePairs.add(new BasicNameValuePair("friend_un", addedusername));
		nameValuePairs.add(new BasicNameValuePair("friend_name", nameDroped));
		nameValuePairs.add(new BasicNameValuePair("category", cat));

		//http post

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Group/RemoveFromCat.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}
		catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//paring data

		try{

			jArray = new JSONArray(result);
			JSONObject json_data=null;


			for(int i=0;i<jArray.length();i++)
			{
				json_data = jArray.getJSONObject(i);
				//status=json_data.getString("status");
				status  = json_data.getString("status");

			}

		}
		catch(JSONException e1)
		{
			Log.e("log_tag", e1.toString());
		} 

	}

	public void setCategoryListView(){
		selectGroups();
		ListView lv = (ListView) findViewById(R.id.listView1);
		catAdapter =new ArrayAdapter<String>(this,R.layout.category, R.id.categorytext,  categories);
		lv.setAdapter(catAdapter);

	}

	//fill the grid views according to clicked category
	public void fillGridView(String cat){

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("userName", GlobalClass.getUsername()));
		//		nameValuePairs.add(new BasicNameValuePair("friend_un", addedusername));
		//		nameValuePairs.add(new BasicNameValuePair("friend_name", nameDroped));
		nameValuePairs.add(new BasicNameValuePair("category", cat));

		//http post

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Group/SelectCatPeople.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}
		catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//paring data

		try{

			jArray = new JSONArray(result);
			JSONObject json_data=null;

			Usernames = new String[jArray.length()];
			Name = new String[jArray.length()];

			for(int i=0;i<jArray.length();i++)
			{
				json_data = jArray.getJSONObject(i);
				//status=json_data.getString("status");
				Usernames[i]  = json_data.getString("friend_un");
				Name[i]  = json_data.getString("friend_name");



			}
			gridView.setAdapter(new ImageAdapter(this, Name,Usernames));
		}
		catch(JSONException e1)
		{
			Log.e("log_tag", e1.toString());
		} 
	}

	//Refresh button click
	public void Refresh(){

		// Creating JSON Parser instance
		JSONParser jParser = new JSONParser();

		// getting JSON string from URL
		JSONObject json = jParser.getJSONFromUrl(url);

		adapter = new ImageAdapter(this, Name,Usernames);
		try {
			// Getting Array of Contacts
			contacts = json.getJSONArray(TAG_CONTACTS);

			Name = new String[contacts.length()];
			Usernames = new String[contacts.length()];
			// looping through All Contacts
			for(int i = 0; i < contacts.length(); i++){
				JSONObject c = contacts.getJSONObject(i);

				// Storing each json item in variable

				String val1 = c.getString(USER_NAME);
				String val2 = c.getString(TAG_NAME);

				Usernames[i]=val1;
				Name[i]=val2;
			}
			gridView.setAdapter(new ImageAdapter(this, Name,Usernames));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Dashboard");
		}

		MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(GroupActivity.this,Dashboard.class));
			this.finish();//
		}

		return true;
		case 1:
			GlobalClass.setUsername("");
			startActivity(new Intent(GroupActivity.this,Login.class));
			this.finish();//

			return true;

		}
		return false;
	}


}
