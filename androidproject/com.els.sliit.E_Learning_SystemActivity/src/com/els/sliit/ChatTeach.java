/**
 * Will handle the chatting interface and connectivity with the openfire server
 */
package com.els.sliit;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.ClipData.Item;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.*;
import org.jivesoftware.smack.packet.*;
import org.jivesoftware.smack.util.StringUtils;

import test.Student;

import chat.MessageThread;

import com.almondmendoza.drawings.DrawingActivity;
import com.els.sliit.R.id;
import com.els.webservices.FileTransfer;
import com.els.webservices.dbAccess;


import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.SpannableString;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemLongClickListener;

public class ChatTeach extends Activity {
	
	
	Bitmap bmpBig;
	Bitmap bitmap;
	private final static String SERVER_HOST = "10.0.2.2";
	private final static int SERVER_PORT = 5222;
	private final static String SERVICE_NAME = "10.0.2.2";	
	private String LOGIN = "";
	private final static String PASSWORD = "123";
	String to = "";
	
	private List<String> m_discussionThread;
	private ArrayAdapter<String> m_discussionThreadAdapter;
	private XMPPConnection m_connection;
	private Handler m_handler;
	
	private List<String> m_people;
	private ArrayAdapter<String> m_peopleAdapter;
	
	private List<String> m_screens;
	private ArrayAdapter<String> m_screensAdapter;
	
	private List<String> m_group;
	
	private List<MessageThread> m_messageThreads;

	protected static boolean isVisible = false;
	
	/**
	 * Called when the activity is started for the first time
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		LOGIN = GlobalClass.getUsername()+"@elearn";
		
		
		
        m_handler = new Handler();
   		try {
   			initConnection();
   		} catch (XMPPException e) {
   			e.printStackTrace();
   		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_activity);
		

		
		// Permits main thread to connect to networks
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		


   		final EditText message = (EditText) this.findViewById(R.id.message);		
   		final ListView list = (ListView) this.findViewById(R.id.thread);
   		
   		m_discussionThread = new ArrayList<String>();
   		m_discussionThreadAdapter = new myArrayAdapter(this,
   				R.layout.chat_item,R.id.txtChatItem, m_discussionThread);
   		
   		//Scroll up the messages list 
   		list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
   		
   		list.setAdapter(m_discussionThreadAdapter);
   		
   		m_discussionThreadAdapter.registerDataSetObserver(new DataSetObserver() {
   		    @Override
   		    public void onChanged() {
   		        super.onChanged();
   		     list.setSelection(m_discussionThreadAdapter.getCount() - 1);    
   		    }
   		});
   		
   		
   		final ListView peopleList = (ListView) this.findViewById(R.id.lstPeople);

   		
   		//Loads list of people into the list view

   		   		
   		//people adapter
   		m_people = new ArrayList<String>();
   		getPeople();
   		m_peopleAdapter = new myPeopleArrayAdapter(this,
   			R.layout.item_people_layout, m_people);
   		
   		peopleList.setAdapter(m_peopleAdapter);
   		
   		
   		//Adds white board history to the list
   		final ListView screenList = (ListView) this.findViewById(R.id.listWBHistory);
   		
   		//screens adapter
   		getWBHistory();
   		
   		m_screensAdapter = new myWBArrayAdapter(this,
   			R.layout.wb_item, m_screens);
   		
   		screenList.setAdapter(m_screensAdapter);

   		//Send button coding
   		Button send = (Button) this.findViewById(R.id.send);
   		send.setOnClickListener(new View.OnClickListener() {
   			public void onClick(View view) {
   				
   				//test object sending
   				/*
   				byte[] stdByte = serializeObject(new Student());
   				
   				Student deserializedStudent = (Student) deserializeObject(stdByte); 
   				
   				final Message msg1 = new Message(to, Message.Type.chat);
   				msg1.setBody(deserializedStudent.id);
   				m_connection.sendPacket(msg1);*/
   				
   				//String to = recipient.getText().toString();
   			   
   				ImageView imgViewWB = (ImageView) findViewById(id.imgAtView);
   				
   				final String imgName = (String) imgViewWB.getTag();
   		
   				final String imgPath = imgName;
   				String text = "";
   				
   				//Checks image is attached or not
   				if(imgName != null)
   				{
   					text = message.getText().toString()+"#"+imgPath;
   				}
   				else
   				{
   					text = message.getText().toString()+"#none";
   				}
   				
   				//Send the message and upload the attached image
   				final Message msg = new Message(to, Message.Type.chat);
   				msg.setBody(text);
   				
   				final ProgressDialog dialog = ProgressDialog.show(ChatTeach.this, "Sending Message", "Please Wait!", true);
   				new Thread(new Runnable(){
      		         public void run() {
    
      		        	
      		        	if(imgName != null)
      		        	{
      		        	FileTransfer fileTransferObject = new FileTransfer();

      		        	fileTransferObject.uploadFile("/sdcard/elearn/wb_history/"+imgName+".png","chatimages/");
      		        	fileTransferObject.uploadFile("/sdcard/elearn/wb_history/"+imgName+"thumb.png","chatimages/");

      		        	}
      		        	
      		        	m_connection.sendPacket(msg);
      		        	
      		          dialog.dismiss();
      		         }
      		     }).start();
   				
   				m_discussionThread.add(LOGIN+"#"+text+"#"+"to");
   				
   				//add to users message thread
   				int position = getMsgThread(to);
   				if(position != -1)
   				{
   					m_messageThreads.get(position).addMessage(LOGIN+"#"+text+"#"+"to");
   					MessageThread msgThread = m_messageThreads.get(position);
   					m_discussionThread.clear();
   					m_discussionThread.addAll(msgThread.getThread());
   				}

   		 		m_discussionThreadAdapter.notifyDataSetChanged();
   		 		
   		 	clearMsg();
   	
   			}
   		});
   		
   		

   		
   	 //Setup the drag drop feature
   	 setupDragDropStuff();
   	 
   	 //Clear button coding
   	 final Button btnClear = (Button) findViewById(id.btnClear); 
   	 btnClear.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {	
			
			clearMsg();
		}
	});
   	 
		//Track the Presence of the users
		
		Roster roster = m_connection.getRoster();
		roster.addRosterListener(new RosterListener() {
		    // Ignored events public void entriesAdded(Collection<String> addresses) {}
		    public void entriesDeleted(Collection<String> addresses) {}
		    public void entriesUpdated(Collection<String> addresses) {}
		    public void presenceChanged(Presence presence) {
		    	//peopleList.refreshDrawableState();
		    	Log.e("openfire","Presence Changed");
		    	//m_peopleAdapter.notifyDataSetChanged();
		    	
				m_handler.post(new Runnable() {
					public void run() {
						m_peopleAdapter.notifyDataSetChanged();
					}
				});
		        
		    }
			@Override
			public void entriesAdded(Collection<String> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		if(!GlobalClass.getThread().isEmpty())
		{
			int position = getMsgThread(to);
			m_discussionThread.clear();
			m_discussionThread.addAll(m_messageThreads.get(position).getThread());
			m_discussionThreadAdapter.notifyDataSetChanged();
		}
		
	}
	
		public byte[] serializeObject(Student o) { 
			    ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
			 
			    try { 
			      ObjectOutput out = new ObjectOutputStream(bos); 
			      out.writeObject(o); 
			      out.close(); 
			 
			      // Get the bytes of the serialized object 
			      byte[] buf = bos.toByteArray(); 
			 
			      return buf; 
			    } catch(IOException ioe) { 
			      Log.e("serializeObject", "error", ioe); 
			 
			      return null; 
			    } 
			  }
		
		public Object deserializeObject(byte[] b) { 
		    try { 
		      ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b)); 
		      Object object = in.readObject(); 
		      in.close(); 
		 
		      return object; 
		    } catch(ClassNotFoundException cnfe) { 
		      Log.e("deserializeObject", "class not found error", cnfe); 
		 
		      return null; 
		    } catch(IOException ioe) { 
		      Log.e("deserializeObject", "io error", ioe); 
		 
		      return null; 
		    } 
		  }
	
	@Override
	public void onResume()
	{
		super.onResume();
		isVisible = true;


		
		m_handler.post(new Runnable() {
			public void run() {

					int position = getMsgThread(to);
					m_discussionThread.clear();
					m_discussionThread.addAll(m_messageThreads.get(position).getThread());

				m_discussionThreadAdapter.notifyDataSetChanged();

				getWBHistory();
				m_screensAdapter.notifyDataSetChanged();

			}
		});
		


	}


	@Override
	public void onPause()
	{
		super.onPause();
		isVisible = false;
	}
	
	@Override
	public void onDestroy()
	{   
	    Cleanup();
	    super.onDestroy();
	}

	private void Cleanup()
	{    
	    bitmap.recycle();
	    bmpBig.recycle();
	    System.gc();
	    Runtime.getRuntime().gc();  
	}

	/**
	 * Get white board history from sd card
	 */
	private void getWBHistory()
	{
		if(m_screens == null)
		{
			m_screens = new ArrayList<String>();
		}
		else
		{
			m_screens.clear();
		}
   		
   		File f=new File("/sdcard/elearn/wb_history"); 
   		
   		String[] images = f.list();
   		
   		
   		for(int i = 0; i < images.length ; i++)
   		{
   			String temp = images[i].toString();
   			String fileName = temp.replaceAll(".png", "");
   			
   			String fileNameClean = fileName.replaceAll("thumb", "");
   			if(!fileName.equals(fileNameClean))
   			{
   				m_screens.add(fileNameClean);
   			}
   		}	
   		
   		f = null;
   		images = null;
	}
	
	private void setupDragDropStuff() {
		
		ListView screenList = (ListView) this.findViewById(R.id.listWBHistory);

        // Drag drop would be triggered once you long tap on a list view's item
		screenList.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View v, int position, long arg3) {
            	   	
                final String title = m_screens.get(position);
                final String textData = title + ":" + position;
                ClipData data = ClipData.newPlainText(title, textData);
                v.startDrag(data, new MyDragShadowBuilder(v), null, 0);
                return true;
            }
        });
		
		ImageView imageViewWB = (ImageView) findViewById(id.imgAtView);

        // Set the Drag Listener to the drop area.
		imageViewWB.setOnDragListener(new OnDragListener() {

            public boolean onDrag(View v, DragEvent event) {
                System.out.println(v.getClass().getName());
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.GRAY);
                        break;

                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;

                    case DragEvent.ACTION_DRAG_STARTED:
                        return processDragStarted(event);

                    case DragEvent.ACTION_DROP:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        return processDrop(event);
                }
                return false;
            }
        });

    }
	
	 /**
     * Process the drop event
     * 
     * @param event
     * @return
     */
    private boolean processDrop(DragEvent event) {
        ClipData data = event.getClipData();
        if (data != null) {
            if (data.getItemCount() > 0) {
                Item item = data.getItemAt(0);
                String textData = (String) item.getText();
                String[] parts = textData.split(":");
                int index = Integer.parseInt(parts[1]);
                String listItem = parts[0];
                updateViewsAfterDropComplete(listItem, index);
                Log.v("test",listItem);
                return true;
            }
        }
        return false;
    }

    /**
     * Update the Views after the drag operation is complete
     * 
     * @param listItem
     * @param index
     */
    
    private void updateViewsAfterDropComplete(String listItem, int index) {
    	
    	ImageView imageViewAttach = (ImageView) findViewById(id.imgAtView);
	    bitmap = BitmapFactory.decodeFile("/sdcard/elearn/wb_history/"+listItem+"thumb.png");
	    imageViewAttach.setImageBitmap(bitmap);

		imageViewAttach.setTag(listItem);
		
		imageViewAttach.setBackgroundColor(Color.WHITE);

    }
    
    /**
     * Check if this is the drag operation you want. There might be other
     * clients that would be generating the drag event. Here, we check the mime
     * type of the data
     * 
     * @param event
     * @return
     */
    
    private boolean processDragStarted(DragEvent event) {
        ClipDescription clipDesc = event.getClipDescription();
        if (clipDesc != null) {
            return clipDesc.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);
        }
        return false;
    }
	
	/**
	 * Get the list of people (Chat List) from the database
	 */
	private void getPeople(){
		m_group = new ArrayList<String>();
		
		dbAccess db = new dbAccess();
		
		//GlobalClass.setCurrentSessionID("26");
		
		if(!GlobalClass.getCurrentSession().isEmpty())
		{
			CharSequence[] members = db.getPeopleForSession(GlobalClass.getCurrentSession());
			
			if(GlobalClass.getUsername().equals(members[0].toString()))
			{
				
				for(int i = 0; i<members.length ; i++)
				{
					if(i != 0)
					{
						if(!GlobalClass.getUsername().equals(members[i].toString()))
							m_group.add(members[i].toString());
					}
				}
			}
			else
			{
				
				m_group.add(members[0].toString());
				
			}

		}
		else
		{
			m_group.add("cjk");
			m_group.add("vida");
			m_group.add("jayamal");
			m_group.add("test1");
		}
		
		if(GlobalClass.getThread().isEmpty())
		{
			m_messageThreads = new ArrayList<MessageThread>();	
		}
		else
		{
			m_messageThreads = new ArrayList<MessageThread>();	
			m_messageThreads.clear();
			m_messageThreads.addAll(GlobalClass.getThread());
		}
		
		Roster roster = m_connection.getRoster();
		roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
		
		
			for(int i = 0; i < m_group.size(); i++)
			{
				try {
					roster.createEntry(m_group.get(i)+"@elearn", m_group.get(i),
							null);
					m_people.add(m_group.get(i)+"@elearn");
					
					if(GlobalClass.getThread().isEmpty())
					{
						MessageThread msgThread = new MessageThread(m_group.get(i)+"@elearn");
						m_messageThreads.add(msgThread);
					}
					
				} catch (XMPPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(!m_people.isEmpty())
			{
				to = m_people.get(0);
			}
		
	}
	
	/**
	 * Gets the correct message thread
	 */
	
	public int getMsgThread(String userID)
	{
		for(int i = 0 ; i < m_messageThreads.size() ; i++ )
		{
			MessageThread msgThread = m_messageThreads.get(i);

			if(msgThread.getUserID().equals(userID))
				return i;			
		}

		return -1;
	}
	
	/**
	 * Generate list items for the chat list
	 */
	private class myPeopleArrayAdapter extends ArrayAdapter<String>{

		public myPeopleArrayAdapter(Context context, int resource,
				 List<String> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View row = inflater.inflate(R.layout.item_people_layout, parent,false);
			
			List<String> people = m_people;
			
			TextView txtUser = (TextView) row.findViewById(id.txtPeopleName);
			RadioButton rButton = (RadioButton) row.findViewById(id.rdStatus);
			final LinearLayout linLayoutBG = (LinearLayout) row.findViewById(id.linLayoutPeopleBG);
			
			//online/offline
			Roster roster = m_connection.getRoster();
			Presence presence;

					presence = roster.getPresence(people.get(position));

					if (presence != null) 
					{
						if(presence.isAvailable() || presence.isAway())
						{
							rButton.setChecked(true);
						}
						else
						{
							rButton.setChecked(false);
						}
					}
		
					if(to.equals(m_people.get(position)))
					{
						linLayoutBG.setBackgroundColor(Color.WHITE);
						txtUser.setTextColor(Color.BLACK);
					}
					else
					{
						linLayoutBG.setBackgroundColor(Color.TRANSPARENT);
						txtUser.setTextColor(Color.WHITE);
					}
					
					txtUser.setText(people.get(position));
					row.setOnClickListener(new OnClickListener(){
						
				        public void onClick(View v) {
				        	
				        	to = m_people.get(position);
				        	
				    		int position = getMsgThread(to);
							if(position != -1)
							{
								MessageThread msgThread = m_messageThreads.get(position);
								m_discussionThread.clear();
								m_discussionThread.addAll(msgThread.getThread());
							}
							m_discussionThreadAdapter.notifyDataSetChanged();		        	
				        	m_peopleAdapter.notifyDataSetChanged();
				        }
						});
			
			return row;
		}

	}
	
	/**
	 * Generate list items for the white board history list
	 */
	private class myWBArrayAdapter extends ArrayAdapter<String>{

		public myWBArrayAdapter(Context context, int resource,
				 List<String> objects) {
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View row = inflater.inflate(R.layout.wb_item, parent,false);
			
			List<String> screens = m_screens;
			
			ImageView imageViewWB = (ImageView) row.findViewById(id.imgWBScreen);
				
			//int img = getResources().getIdentifier(screens.get(position), "drawable", "com.els.sliit");
			
			//imageViewWB.setImageDrawable(getResources().getDrawable(img));

		    bitmap = BitmapFactory.decodeFile("/sdcard/elearn/wb_history/"+screens.get(position)+"thumb.png");
		    imageViewWB.setImageBitmap(bitmap);
	
			return row;
		}

		     private Drawable LoadImageFromWebOperations(String url)
		    {
		         try
		         {
		             InputStream is = (InputStream) new URL(url).getContent();
		             Drawable d = Drawable.createFromStream(is, "src name");
		             return d;
		         }catch (Exception e) {
		             System.out.println("Exc="+e);
		             return null;
		         }
		     }

	}
	
	
	/**
	 * Generate list items for the messages list view
	 */
	private class myArrayAdapter extends ArrayAdapter<String>{

		public myArrayAdapter(Context context, int resource,
				int textViewResourceId, List<String> objects) {
			super(context, resource, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View row = inflater.inflate(R.layout.chat_item, parent,false);
			
			List<String> msgs = m_discussionThread;
			
			TextView txtMsg = (TextView) row.findViewById(id.txtChatItem);
			TextView txtUnm = (TextView) row.findViewById(id.txtChatName);
			TextView txtUnmSuff = (TextView) row.findViewById(id.txtChatNameSuffix);
			LinearLayout linLayImageBG = (LinearLayout) row.findViewById(id.llImageBg);
			
			txtMsg.setText(msgs.get(position));
			
			String[] msgArray = msgs.get(position).split("#");
			
			 final String name;
			 final String body ;
			 final String picture ;
			 final String tag ;
			 
	
			if(msgArray.length == 4)
			{
			
			  name = msgArray[0];
			  body = msgArray[1];
			  picture = msgArray[2];
			  tag = msgArray[3];
			}
			else
			{
				  name = msgArray[0];
				  body = msgArray[1];
				  tag = msgArray[2];
				  picture = "none";
			}
			 
			 if(!picture.equals("none"))
			 {
				 ImageView imgView = (ImageView) row.findViewById(id.imgBtnWB);
				 
				 //download image from web serve
				 final FileTransfer fileTransferObj = new FileTransfer();
				 
				 if(tag.equals("to"))
				 {
					 bitmap = fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+GlobalClass.getUsername()+"/"+picture+"thumb.png");
				//	 bmpBig = fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+GlobalClass.getUsername()+"/"+picture+".png");
					 
					 if(bitmap != null)
					 imgView.setImageBitmap(bitmap);
				 }
				 else
				 {
					 String[] userName = name.split("@");
					 bitmap = fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+userName[0]+"/"+picture+"thumb.png");
				//	 bmpBig =  fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+userName[0]+"/"+picture+".png");
						
					 if(bitmap != null)
					 imgView.setImageBitmap(bitmap);
				 }
				 
				 imgView.setOnClickListener(new OnClickListener(){
						
				        public void onClick(View v) {
				        	
				        	//set up dialog
				            final Dialog dialog = new Dialog(ChatTeach.this);
				            dialog.setContentView(R.layout.pic_view_dialog);
				            dialog.setTitle(body);
				            dialog.setCancelable(true);
				            //there are a lot of settings, for dialog, check them all out!

				            if(tag.equals("to"))
				            {
				            	bmpBig = fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+GlobalClass.getUsername()+"/"+picture+".png");
								
				            }
				            else
				            {
				            	String[] userName = name.split("@");
				            	bmpBig =  fileTransferObj.fetchPicture("http://10.0.2.2:1000/eLearningWebServices/Common/chatimages/"+userName[0]+"/"+picture+".png");
				            }

				            //set up image view
				            ImageView img = (ImageView) dialog.findViewById(R.id.imgWBView);
				          
				            img.setImageBitmap(bmpBig);

				            //set up button
				            Button button = (Button) dialog.findViewById(R.id.imgViewButtonDialog);
				            button.setOnClickListener(new OnClickListener() {
				            @Override
				                public void onClick(View v) {
				            	
				            	dialog.dismiss();
				            	bmpBig.recycle();
				                }
				            });
				            //now that the dialog is set up, it's time to show it    
				            dialog.show();

				        }
						});
					

			 }
			 else
			 {
				 ImageView imgView = (ImageView) row.findViewById(id.imgBtnWB);
				 imgView.setVisibility(imgView.GONE);
				 
			 }
			 
			 if(tag.equals("to"))
			 {
				 	txtUnm.setText(name);
				 	txtUnmSuff.setText(" says:");
					txtMsg.setText(body);
					
					txtUnm.setTextColor(Color.BLACK);
					txtUnmSuff.setTextColor(Color.BLACK);
					
					txtUnm.setBackgroundColor(Color.LTGRAY);
					txtUnmSuff.setBackgroundColor(Color.LTGRAY);
					
					txtMsg.setTextColor(Color.BLACK);
					txtMsg.setBackgroundColor(Color.LTGRAY);
					
					linLayImageBG.setBackgroundColor(Color.GRAY);
			 }
			 else
			 {
				 	txtUnm.setText(name);
				 	txtUnmSuff.setText(" says:");
					txtMsg.setText(body);
					
					txtUnm.setTextColor(Color.WHITE);
					txtUnmSuff.setTextColor(Color.WHITE);
					
					txtUnm.setBackgroundColor(Color.DKGRAY);
					txtUnmSuff.setBackgroundColor(Color.DKGRAY);
					
					txtMsg.setTextColor(Color.WHITE);
					txtMsg.setBackgroundColor(Color.DKGRAY);
					
					linLayImageBG.setBackgroundColor(Color.GRAY);
			 }
			 
			 if(position == 0)
			 GlobalClass.setMsgThread(m_messageThreads);
			 

			return row;
		}	
	}
	
	/**
	 * Establish the connectivity between client and the chat server
	 */
	private void initConnection() throws XMPPException {
		//Initialisation of the connection
        ConnectionConfiguration config =
                new ConnectionConfiguration(SERVER_HOST, SERVER_PORT, SERVICE_NAME);
        m_connection = new XMPPConnection(config);
        m_connection.connect();
        m_connection.login(LOGIN, PASSWORD);
        
        GlobalClass.setConnection(m_connection);
        
        Presence presence = new Presence(Presence.Type.available);
        m_connection.sendPacket(presence);
       
        //Receive messages forn the chat server
		PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
		m_connection.addPacketListener(new PacketListener() {
				public void processPacket(Packet packet) {
					Message message = (Message) packet;
					if (message.getBody() != null) {
						
						String msgFilter[] = message.getBody().split("#");
						
						//if(!msgFilter[0].equalsIgnoreCase("[1"))
						//{
						
						final String fromName = StringUtils.parseBareAddress(message
								.getFrom());
						//m_discussionThread.add(fromName + "#" + message.getBody() + "#" + "from");
						
						String userID = "";
						
						//add to users message thread
						int position = getMsgThread(fromName);
						if(position != -1)
						{
							m_messageThreads.get(position).addMessage(
									fromName + "#" + message.getBody() + "#"
											+ "from");
							MessageThread msgThread = m_messageThreads
									.get(position);
							userID = msgThread.getUserID();
							
						
							final String msgNF = message.getBody();
							

							
							if(fromName.equals(to) && isVisible)
							{

								m_discussionThread.clear();
								m_discussionThread.addAll(msgThread.getThread());
								
								m_handler.post(new Runnable() {
									public void run() {
										m_discussionThreadAdapter.notifyDataSetChanged();
									}
								});
							}
							else
							{
								m_handler.post(new Runnable() {
									public void run() {
										String ns = Context.NOTIFICATION_SERVICE;
										NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
										int icon = R.drawable.ic_action_mail;
										CharSequence tickerText = "New Message!";
										long when = System.currentTimeMillis();

										Notification notification = new Notification(icon, tickerText, when);
									
										Context context = getApplicationContext();
										CharSequence contentTitle = "New message from "+fromName;
										CharSequence contentText = msgNF;
										Intent notificationIntent = new Intent(context, ChatTeach.class);
										PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

										notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
										
										final int HELLO_ID = 1;

										mNotificationManager.notify(HELLO_ID, notification);
										
										Toast.makeText(context, "New message from "+fromName , Toast.LENGTH_LONG).show();
									}
								});	
							}
						//}
						
						}
					}
				}
			}, filter);
		
		
        //GlobalClass.setConnection(m_connection);
	}
	
	/**
	 * Clears the message input fields and attached image
	 */
	private void clearMsg(){
		
		EditText message = (EditText) this.findViewById(R.id.message);
		ImageView imgView = (ImageView)this.findViewById(id.imgAtView);
		
		message.setText("");
		imgView.setTag(null);
		
		int img = getResources().getIdentifier("ic_attachment", "drawable", "com.els.sliit");
		
		imgView.setImageDrawable(getResources().getDrawable(img));
		
	}
	
	/** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Dashboard");
		}

		MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}
		
		MenuItem mnu3 = menu.add(0, 2, 2, "White Board");
		{
			mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu3.setIcon(R.drawable.ic_menu_exit);
			mnu3.setTitle("White Board");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(ChatTeach.this,Dashboard.class));
		}

		return true;
		case 1:
		{
			GlobalClass.setUsername("");
			//m_connection.disconnect();
			GlobalClass.setCurrentSessionID("");
			GlobalClass.clearThread();
			startActivity(new Intent(ChatTeach.this,Login.class));

		}
			return true;
			
		case 2:
		{
			Intent intent = new Intent(ChatTeach.this, DrawingActivity.class);
		//	intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
		}
			return true;
		}
		return false;
	}



}
