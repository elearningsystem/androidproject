//E-Learning Environment
//addmemberstogroups.java 
//Handles activities related to 
//adding members to the groups
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class addmemberstogroups extends Activity {
	
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb = null;
	String type="";
	String strSName="";
	String strSDescription="";
	String groupID="";
	String groupName="";
	String access="";
	String logic="";
	private ArrayList <HashMap<String, Object>> myContent;
	private static final String NAME = "name";
	private static final String UNAME = "uname";
	private static final String OPERATION = "operation";
	String CUNAME = "cuname";
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
         setContentView(R.layout.addmemberstosession);
         groupID = getIntent().getExtras().getString("gid");
         groupName = getIntent().getExtras().getString("gname");
         access = getIntent().getExtras().getString("type");
         
         
         //changing security policy
         StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
         StrictMode.setThreadPolicy(policy);
         
         GetMembers("http://10.0.2.2:1000/test/getMembers.php",1,"","");
         
         //Back button click event
        Button back = (Button)findViewById(R.id.btnSearchNameDone);
         back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent notificationIntent = new Intent(addmemberstogroups.this,deletesessions.class);
				   startActivity(notificationIntent);
				   finish();//
			}
		});
         
         //search button click event
      //   Button btnSearch = (Button)findViewById(R.id.btnSearchName);
        // EditText etKeyword = (EditText)findViewById(R.id.etsearchName);
         /* final String keyword = etKeyword.getText().toString();
        btnSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), access ,Toast.LENGTH_LONG).show();
				// TODO Auto-generated method stub
			if(logic.equals("Priv"))
				{
				GetMembers("http://10.0.2.2/test/searchAllMembers.php",2,"accepted",keyword);
	       
				}
				else{
		        	GetMembers("http://10.0.2.2/test/searchAllMembers.php",2,"pending",keyword);
			
			}
		}); 
		
		*/
    }
    
    /** Getting the full name of the user. */
    public void GetName(String url)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        //convert response to string
        if(stat==1)
        {
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
    
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     CUNAME=json_data.getString("name");
                  
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        }
    }
    
    /** Adding members to the groups. */
    public void AddToGroup(String username,String mode) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", username));
		nameValuePairs.add(new BasicNameValuePair("gid", groupID));
		nameValuePairs.add(new BasicNameValuePair("status", mode));
		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://10.0.2.2:1000/test/addToGroups.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		// paring data
		String status = null;
		try {

			jArray = new JSONArray(result);
			JSONObject json_data = null;
			for (int i = 0; i < jArray.length(); i++) {
				json_data = jArray.getJSONObject(i);
				status = json_data.getString("status");

			}
			if ((status.equals("Error")))
				Toast.makeText(getBaseContext(), "Cannot add to the group",Toast.LENGTH_LONG).show();

			else {
				Toast.makeText(getBaseContext(), "Notification Sent",Toast.LENGTH_LONG).show();
				
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "JError", Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}
	}
    
    /** Sending a notification to a user with relevant group details. */
    public void SendNotification(String notifier,String sender,String type,String status,String gid,String gname,String sendername)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
    
      
        //convert response to string
        if(stat==1)
        {
            nameValuePairs.add(new BasicNameValuePair("notifier",notifier));
        	nameValuePairs.add(new BasicNameValuePair("sender",GlobalClass.getUsername().toString()));
        	nameValuePairs.add(new BasicNameValuePair("type",type));
        	nameValuePairs.add(new BasicNameValuePair("status",status));
        	nameValuePairs.add(new BasicNameValuePair("gid",gid));
        	nameValuePairs.add(new BasicNameValuePair("gname",gname));
        	nameValuePairs.add(new BasicNameValuePair("sendername",CUNAME));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://10.0.2.2:1000/test/addNotification.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	listView = (ListView)findViewById(R.id.lvSearchName);
        	listView.setClickable(true);
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }      
        }
    }

    
    public void GetMembers(String url,int status,String para,String para2)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=status;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        try{
             HttpClient httpclient = new DefaultHttpClient();
             HttpPost httppost = new HttpPost(url);
             httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
             HttpResponse response = httpclient.execute(httppost);
             HttpEntity entity = response.getEntity();
             is = entity.getContent();
             }catch(Exception e){
                 Log.e("log_tag", "Error in http connection"+e.toString());
            }
        //convert response to string
        if(stat==1)
        {
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
        	nameValuePairs.add(new BasicNameValuePair("gid",groupID));
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
        	nameValuePairs.add(new BasicNameValuePair("status",para));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	listView = (ListView)findViewById(R.id.lvSearchName);
        	listView.setClickable(true);
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        String name;
        String uName;
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     name=json_data.getString("friend_name");
                     uName=json_data.getString("friend_un");
                     hm = new HashMap<String, Object>();
                     hm.put(NAME, name);
                     hm.put(UNAME, uName);
                     myContent.add(hm);
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        listView.setAdapter(new myListAdapter(myContent,this,1));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
        
        else if(stat==2)
        {
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
        	nameValuePairs.add(new BasicNameValuePair("gid",groupID));
        	nameValuePairs.add(new BasicNameValuePair("status",para));
        	nameValuePairs.add(new BasicNameValuePair("para2",para2));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	listView = (ListView)findViewById(R.id.lvSearchName);
        	listView.setClickable(true);
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        String name;
        String uName;
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     name=json_data.getString("friend_name");
                     uName=json_data.getString("friend_un");
                     hm = new HashMap<String, Object>();
                     hm.put(NAME, name);
                     hm.put(UNAME, uName);
                     myContent.add(hm);
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        listView.setAdapter(new myListAdapter(myContent,this,1));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    /** Adding received data to a holder to display in controls. */ 
 private class myListAdapter extends BaseAdapter{
    	
    	
    	private ArrayList<HashMap<String, Object>> Content; 
    	private LayoutInflater mInflater;
    	int status;
    	
    	
		public myListAdapter(ArrayList<HashMap<String, Object>> content, Context context,int stat){
			status=stat;
			Content = content;
			mInflater = LayoutInflater.from(context);
		}
    	
    	
    	public int getCount() {
			// TODO Auto-generated method stub
			return Content.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Content.get(position);
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if(status==1)
			{
			// TODO Auto-generated method st
			// A ViewHolder keeps references to children views to avoid unneccessary calls
            // to findViewById() on each row.
			ViewHolder holder;
			
			// When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null
			
			 if (convertView == null) {
	             convertView = mInflater.inflate(R.layout.getuserlv, null);
	             // Creates a ViewHolder and store references to the two children views
	             // we want to bind data to.
	             
	             holder = new ViewHolder();
	             holder.name = (TextView) convertView.findViewById(R.id.tvMemberName);
	             holder.uName = (TextView) convertView.findViewById(R.id.tvMemberUname);
	             convertView.setTag(holder);
	                
			 }else {
				 // Get the ViewHolder back to get fast access to the views
				 holder = (ViewHolder) convertView.getTag(); 
			 }
			 	// Bind the data holder.
			 
				holder.name.setText((String) Content.get(position).get(NAME));
				
				holder.uName.setText((String) Content.get(position).get(UNAME));				
				
				convertView.setOnClickListener(new OnClickListener(){
					
			        public void onClick(View v) {	
			        	
			        	
			        }
			        
			        
			    });
				Button add = (Button)convertView.findViewById(R.id.btngetuserlvop);
	        	TextView name = (TextView)convertView.findViewById(R.id.tvMemberName);
	        	TextView uname = (TextView)convertView.findViewById(R.id.tvMemberUname);
	        	String strName = name.getText().toString();
	        	final String strUName = uname.getText().toString();
	        	final String strNameU = name.getText().toString();
	        	/*if(access.equals("Private"))
	        		add.setText("Send Request");
	        	else
	        		add.setText("Add");*/
	        	add.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(access.equals("Private"))
						{
						AddToGroup(strUName,"pending");
			        	GetMembers("http://10.0.2.2:1000/test/getMembers.php",1,"accepted","");
			        	GetName("http://10.0.2.2:1000/test/getCUserName.php");
			        	SendNotification(strUName,GlobalClass.getUsername().toString(),"creq","1",groupID,groupName,CUNAME);
						}
						else{
							AddToGroup(strUName,"accepted");
				        	GetMembers("http://10.0.2.2:1000/test/getMembers.php",1,"pending","");
						}
					}
				});
				
			}
			return convertView;
		}
		class ViewHolder {
			TextView name;
	 	    TextView uName;
	 	    Button operation;
	 	    
	 }
 }
 
 /** Menu item click event */
 @Override
 public boolean onOptionsItemSelected(MenuItem item)
 {
 	return MenuChoice(item);
 }

 /** Excute tasks for click events */
 private boolean MenuChoice(MenuItem item)
 {
 	switch (item.getItemId()) {
 	case 0:
 	{
 		startActivity(new Intent(addmemberstogroups.this,Dashboard.class));
 		this.finish();//
 	}

 	return true;
 	case 1:
 		GlobalClass.setUsername("");
 		startActivity(new Intent(addmemberstogroups.this,Login.class));
 		this.finish();//

 		return true;

 	}
 	return false;
 }
}
