//E-Learning Environment
//FinalNotiActivity.java 
//Works as the base class of the session management
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class FinalNotiActivity extends Activity {
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb = null;
	String type="";
	String strSName="";
	String access="";
	String strSDescription="";
	String groupID="";
	// I use HashMap arraList which takes objects
		private ArrayList <HashMap<String, Object>> myContent;
		private static final String NAME = "name";
		private static final String UNAME = "uname";
		View viewAddMembers=null;
		View viewAddMembers1=null;
		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_dash);
        
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
       
        Intent myIntent = new Intent(getApplicationContext(), ServiceTemplate.class);
        myIntent.putExtra("extraData", "somedata");
        startService(myIntent); 
        
        //inflating a layout 
        LayoutInflater inflater = LayoutInflater.from(this);            
        final View textEntryView = inflater.inflate(R.layout.addsession, null);
        
        //inflating a layout
        LayoutInflater addMembers = LayoutInflater.from(this);            
        viewAddMembers = addMembers.inflate(R.layout.addmemberstosession, null);
        
        //inflating a layout
        LayoutInflater addMembers1 = LayoutInflater.from(this);          
        viewAddMembers1 = addMembers1.inflate(R.layout.getuserlv, null);
        
        final ListView mode = new ListView(FinalNotiActivity.this);
        final ArrayList<String>modeItems = new ArrayList<String>();
        final ArrayAdapter<String>arrayAdapter;
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,modeItems);
        mode.setAdapter(arrayAdapter);
        modeItems.add("Public");
        modeItems.add("Private");
        final EditText sName = (EditText)textEntryView.findViewById(R.id.sName);
        final EditText sDescription = (EditText)textEntryView.findViewById(R.id.sDescription);
        Button sCreate = (Button)textEntryView.findViewById(R.id.btnsCreate);
        Button sCancel = (Button)textEntryView.findViewById(R.id.btnsCancel);
        Button searchNameDone = (Button)viewAddMembers.findViewById(R.id.btnSearchNameDone);
        
        //Button search click event
        Button search = (Button) findViewById(R.id.searchsession);
        search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(FinalNotiActivity.this, search_groups.class));
				
			}
		});
        
        
    //    final EditText etSearchName = (EditText)viewAddMembers.findViewById(R.id.etsearchName);
  //      Button btnSearchName = (Button)viewAddMembers.findViewById(R.id.btnSearchName);
        ListView lvSearchName = (ListView)viewAddMembers.findViewById(R.id.lvSearchName);
        final AlertDialog alertSearch;
        final AlertDialog.Builder alertSearch2 = new AlertDialog.Builder(FinalNotiActivity.this);
        alertSearch2.setTitle("Select Members");
        alertSearch2.setView(viewAddMembers);
        alertSearch=alertSearch2.create();
        
        //creating a alert dialogue to get a session type from the user
        final AlertDialog alert;
        final AlertDialog.Builder myDialog = new AlertDialog.Builder(FinalNotiActivity.this);
        myDialog.setTitle("Create Group");
        myDialog.setMessage("Select session type");
        myDialog.setView(mode);
        myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				
				
			}
		});
        alert=myDialog.create();
        
        Button addSession = (Button)findViewById(R.id.addsession);
        addSession.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
		        alert.show();
		        
				
			}
		});
        //manage session button click event. Loads delete session activity
        Button manageSession = (Button)findViewById(R.id.managesession);
        manageSession.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				startActivity(new Intent(FinalNotiActivity.this, deletesessions.class));
				finish();//
		        
				
			}
		});
        
        //creating an alert dialogue to get group details from the user
        final AlertDialog alertGetData;
        final AlertDialog.Builder getData = new AlertDialog.Builder(FinalNotiActivity.this);
        getData.setTitle("Enter Details");
        getData.setView(textEntryView);
		alertGetData=getData.create();
        mode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    	public void onItemClick(AdapterView<?>arg0,View arg1,int position,long arg3) {
    		//Toast.makeText(getApplicationContext(), "Password fields are empty!", Toast.LENGTH_SHORT).show(); 
    		type=mode.getItemAtPosition(position).toString();
    		access=type;
    		alertGetData.show();
    		alert.dismiss();
    	}
	});
        //search button click event
        searchNameDone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ListView list = (ListView)viewAddMembers.findViewById(R.id.lvSearchName);
				TextView tv = (TextView)viewAddMembers1.findViewById(R.id.tvMemberName);

				int count = list.getAdapter().getCount();

				for (int i = 0; i < count; i++) {

					if (list.isItemChecked(i)) {
						Toast.makeText(getBaseContext(),tv.getText().toString(),Toast.LENGTH_LONG).show();
					}

				
			}
			}
		});
        
        sCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertGetData.dismiss();
			}
		});
        
        sCreate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strSName = sName.getText().toString(); 
				strSDescription = sDescription.getText().toString();
				CreateGroup(strSName,strSDescription,type);
				alertGetData.dismiss();
				sName.setText("");
				sDescription.setText("");
				//GetMembers("http://10.0.2.2/test/getMembers.php",1,"");
				
				
			}
		});
        
        
    }
    
    /** Adding users to groups. */
    public void AddToGroup(String username) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", username));
		nameValuePairs.add(new BasicNameValuePair("gid", groupID));
		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://10.0.2.2:1000/test/addToGroups.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		// paring data
		String status = null;
		try {

			jArray = new JSONArray(result);
			JSONObject json_data = null;
			for (int i = 0; i < jArray.length(); i++) {
				json_data = jArray.getJSONObject(i);
				status = json_data.getString("status");

			}
			if ((status.equals("Error")))
				Toast.makeText(getBaseContext(), "Cannot add to the group",Toast.LENGTH_LONG).show();

			else {
				Toast.makeText(getBaseContext(), "Notification Sent",Toast.LENGTH_LONG).show();
				
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "JError", Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}
	}
    
    /** Creating a group by providing necessary details. */
    public void CreateGroup(String gName, String gDescription,String type) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", GlobalClass.getUsername().toString()));
		nameValuePairs.add(new BasicNameValuePair("group_name", gName));
		nameValuePairs.add(new BasicNameValuePair("description", gDescription));
		nameValuePairs.add(new BasicNameValuePair("type", type));
		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://10.0.2.2:1000/test/addGroups.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		// paring data
		String status = null;
		try {

			jArray = new JSONArray(result);
			JSONObject json_data = null;
			for (int i = 0; i < jArray.length(); i++) {
				json_data = jArray.getJSONObject(i);
				status = json_data.getString("status");

			}
			if ((status.equals("Error")))
				Toast.makeText(getBaseContext(), "Cannot create the group",Toast.LENGTH_LONG).show();

			else {
				
				groupID = status;
				AlertDialog.Builder addAlert = new AlertDialog.Builder(FinalNotiActivity.this);	
				addAlert.setTitle("Alert");
				addAlert.setMessage("Do you want to add members?");
				addAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					final String access1=access;
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						   Intent notificationIntent = new Intent(FinalNotiActivity.this,addmemberstogroups.class);
						   notificationIntent.putExtra("gid", groupID);
						   notificationIntent.putExtra("type", access1);
						   startActivity(notificationIntent);
						   finish();//
					}
				});
				addAlert.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				addAlert.show();
				
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "JError", Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}
	}
    
    /** Getting members who are friends of a particular user. */
    public void GetMembers(String url,int status,String para)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=status;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        try{
             HttpClient httpclient = new DefaultHttpClient();
             HttpPost httppost = new HttpPost(url);
             httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
             HttpResponse response = httpclient.execute(httppost);
             HttpEntity entity = response.getEntity();
             is = entity.getContent();
             }catch(Exception e){
                 Log.e("log_tag", "Error in http connection"+e.toString());
            }
        //convert response to string
        if(stat==1)
        {
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
        	nameValuePairs.add(new BasicNameValuePair("gid",groupID));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	listView = (ListView)viewAddMembers.findViewById(R.id.lvSearchName);
        	listView.setClickable(true);
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        String name;
        String uName;
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     name=json_data.getString("friend_name");
                     uName=json_data.getString("friend_un");
                     hm = new HashMap<String, Object>();
                     hm.put(NAME, name);
                     hm.put(UNAME, uName);
                     myContent.add(hm);
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        listView.setAdapter(new myListAdapter(myContent,this,1));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
    
    /** Adding received data to a holder to display in controls. */ 
 private class myListAdapter extends BaseAdapter{
    	
    	
    	private ArrayList<HashMap<String, Object>> Content; 
    	private LayoutInflater mInflater;
    	int status;
    	
    	
		public myListAdapter(ArrayList<HashMap<String, Object>> content, Context context,int stat){
			status=stat;
			Content = content;
			mInflater = LayoutInflater.from(context);
		}
    	
    	
    	public int getCount() {
			// TODO Auto-generated method stub
			return Content.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Content.get(position);
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if(status==1)
			{
			// TODO Auto-generated method st
			// A ViewHolder keeps references to children views to avoid unneccessary calls
            // to findViewById() on each row.
			ViewHolder holder;
			
			// When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null
			
			 if (convertView == null) {
	             convertView = mInflater.inflate(R.layout.getuserlv, null);
	             // Creates a ViewHolder and store references to the two children views
	             // we want to bind data to.
	             
	             holder = new ViewHolder();
	             holder.name = (TextView) convertView.findViewById(R.id.tvMemberName);
	             holder.uName = (TextView) convertView.findViewById(R.id.tvMemberUname);	             
	             convertView.setTag(holder);
	                
			 }else {
				 // Get the ViewHolder back to get fast access to the views
				 holder = (ViewHolder) convertView.getTag(); 
			 }
			 	// Bind the data with the holder.
			 
				holder.name.setText((String) Content.get(position).get(NAME));
				
				holder.uName.setText((String) Content.get(position).get(UNAME));
				
				convertView.setOnClickListener(new OnClickListener(){
					
			        public void onClick(View v) {
			        	TextView username = (TextView)v.findViewById(R.id.tvMemberUname);
			        	String strUserName = username.getText().toString();
			        	GetMembers("http://10.0.2.2:1000/test/getMembers.php",1,"");
			        	AddToGroup(strUserName);
			        	
			        	
			        }
			        
			        
			    });
				
			}
			return convertView;
		}
		class ViewHolder {
			TextView name;
	 	    TextView uName;
	 	    
	 }
 }
 
/** Add dashboard menu item to action bar. */
@Override
public boolean onCreateOptionsMenu(Menu menu) {
	super.onCreateOptionsMenu(menu);
	CreateMenu(menu);
	return true;
}

/** Create the menu */
private void CreateMenu(Menu menu)
{
	MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
	{

		mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
				MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		mnu1.setIcon(R.drawable.ic_menu_largetiles);
		mnu1.setTitle("Dashboard");
	}

	MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
	{
		mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
				MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		mnu2.setIcon(R.drawable.ic_menu_exit);
		mnu2.setTitle("Logout");
	}

}

/** Menu item click event */
@Override
public boolean onOptionsItemSelected(MenuItem item)
{
	return MenuChoice(item);
}

/** Excute tasks for click events */
private boolean MenuChoice(MenuItem item)
{
	switch (item.getItemId()) {
	case 0:
	{
		startActivity(new Intent(FinalNotiActivity.this,Dashboard.class));
		this.finish();//
	}

	return true;
	case 1:
		GlobalClass.setUsername("");
		startActivity(new Intent(FinalNotiActivity.this,Login.class));
		this.finish();//

		return true;

	}
	return false;
}

 
}