/*
* WebConnectivity
*
* Version 1.0
*
* Establish communication with the web server
*/
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ParseException;
import android.util.Log;
import android.widget.Toast;

/**
 * The WebConnectivity class provide methods to to request a service
 * from the web server
 */
public class WebConnectivity {
	
	String serverIp = "10.0.2.2:1000";
	
	//Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
    
	/** HTTP Post without parameters */
	public InputStream httpPost(String url)
	{
		String fullUrl = "http://"+serverIp+"/"+url;
		
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(fullUrl);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}
		return is;	
	}
	
	/** HTTP Post with extra parameters */
	public InputStream httpPost(String url,ArrayList<NameValuePair> nameValuePairs)
	{
		String fullUrl = "http://"+serverIp+"/"+url;
		
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(fullUrl);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection"+e.toString());
		}
		return is;	
	}
	
	/** Convert InputStream in to a String */
	public String convertResponse(InputStream inputStream)
	{
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line="0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			inputStream.close();
			result=sb.toString();
			//Log.e("log_tag", result.toString());
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		return result;
	}
	
	/** Convert String in to ArrayList */
	public ArrayList <HashMap<String, Object>> getArrayList(String id, String result, String Key)
	{
		ArrayList <HashMap<String, Object>> myContent = null;
		String value = "";
		HashMap<String, Object> hm;
        try{
         	 
            jArray = new JSONArray(result);
            JSONObject json_data=null;
            for(int i=0;i<jArray.length();i++){
                   json_data = jArray.getJSONObject(i);
                   value=json_data.getString(Key);
                   hm = new HashMap<String, Object>();
                   hm.put(id, value);
                   myContent.add(hm);
               }
            }
            catch(JSONException e1){
          	  Log.e("log_tag", e1.toString());
            } catch (ParseException e1) {
      			e1.printStackTrace();
      	}
        
		return myContent;
	
	}
	

}
