/*
 * DirectoryBrowser
 *
 * Version 1.0
 *
 * Enables user to browse files inside the phone storage
 */
package com.els.sliit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DirectoryBrowser extends ListActivity {

	private List<String> item = null;
	private List<String> path = null;
	private String root = "/";
	private TextView myPath;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_br_fragment);

		ActionBar acBar = getActionBar();
		acBar.setTitle("Browse content");

		myPath = (TextView) findViewById(R.id.path);
		getDir(root);

	}

	/** Gets list of files and directories in the given path */
	private void getDir(String dirPath) {
		myPath.setText("Location: " + dirPath);

		item = new ArrayList<String>();
		path = new ArrayList<String>();

		File f = new File(dirPath);
		File[] files = f.listFiles();

		if (!dirPath.equals(root)) {

			item.add(root);
			path.add(root);

			item.add("../");
			path.add(f.getParent());

		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			path.add(file.getPath());
			if (file.isDirectory())
				item.add(file.getName() + "/");
			else
				item.add(file.getName());
		}

		//Add directories and files to the list view
		ArrayAdapter<String> fileList = new ArrayAdapter<String>(this,
				R.layout.file_list_row, item);
		setListAdapter(fileList);

	}

	/** list view item click event */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		File file = new File(path.get(position));

		if (file.isDirectory()) {
			if (file.canRead())
				getDir(path.get(position));
			else {
				new AlertDialog.Builder(this)
						.setIcon(R.drawable.ic_launcher)
						.setTitle(
								"[" + file.getName()
										+ "] folder can't be read!")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

								
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
									}
								}).show();
			}
		} else {

			final String path = file.getAbsolutePath();
			new AlertDialog.Builder(this)
					.setIcon(R.drawable.ic_launcher)
					.setTitle("[" + file.getName() + "]")
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

							
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							})
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

						
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									GlobalClass.setFilePath(path);
								
									//Go to Upload Content
									startActivity(new Intent(
											DirectoryBrowser.this,
											UploadContent.class));
									finish();//
								}
							}).show();
		}
	}

	/** Add Share Content menu item to action bar */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	private void CreateMenu(Menu menu) {
		MenuItem mnu1 = menu.add(0, 0, 0, "Share Content");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
					| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Share Content");
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0: {
			startActivity(new Intent(DirectoryBrowser.this, UploadContent.class));
			this.finish();//
		}
			return true;

		}
		return false;
	}
}
