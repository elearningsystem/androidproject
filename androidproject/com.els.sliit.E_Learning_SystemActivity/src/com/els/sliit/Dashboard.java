/*
 * Dashboard
 *
 * Version 1.0
 *
 * Shows links to the main functions of the application
 */

package com.els.sliit;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class Dashboard extends Activity {
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard_fragment);

		ActionBar acBar = getActionBar();
		acBar.setTitle("Dashboard");

		Toast.makeText(getApplicationContext(),
				"Welcome " + GlobalClass.getUsername(), Toast.LENGTH_LONG)
				.show();

		// btnShare click event
		Button btnShare = (Button) findViewById(R.id.btnShare);

		btnShare.setOnClickListener(new View.OnClickListener() {

	
			public void onClick(View v) {

				// Go to Share View
				startActivity(new Intent(Dashboard.this, ShareView.class));
				finish();//

			}
		});

		// btnShare click event
		Button btnProfile = (Button) findViewById(R.id.btnProfile);

		btnProfile.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				// Go to Share View
				startActivity(new Intent(Dashboard.this, Profile_Update.class));
				finish();//

			}
		});
		
		Button btnGroup = (Button) findViewById(R.id.btnSettings);
		
		btnGroup.setOnClickListener(new OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Dashboard.this, GroupActivity.class));
				finish();//
			}
		});
		
       Button btnTeach = (Button) findViewById(R.id.btnTeach);
		
		btnTeach.setOnClickListener(new OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Dashboard.this, FinalNotiActivity.class));
				finish();//
			}
		});
		
        Button btnLearn = (Button) findViewById(R.id.btnLearn);
		
		btnLearn.setOnClickListener(new OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Dashboard.this, deletesessions.class));
				finish();//
			}
		});
		
        Button btnChat = (Button) findViewById(R.id.btnCollaborate);
		
        btnChat.setOnClickListener(new OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				Intent intent = new Intent(Dashboard.this, ChatTeach.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();//
			}
		});
		
       

	}
	


	/** Add Logout menu item to action bar */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	private void CreateMenu(Menu menu) {

		MenuItem mnu2 = menu.add(0, 0, 0, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
					| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item) {
		switch (item.getItemId()) {
		case 0: {
			startActivity(new Intent(Dashboard.this, Login.class));
			this.finish();//
		}
		return true;

		}
		return false;
	}

}
