//E-Learning Environment
//ServiceTemplate.java 
//working as a background service in a different 
//thread to check whether notifications are available
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

public class ServiceTemplate extends Service {
	private ArrayList <HashMap<String, Object>> myContent;
	
	CharSequence title = "Notification title";
	   CharSequence text = "Notification description";
	   CharSequence statusc = "Notification description";
	Handler mHandler = new Handler();
	int count = 0;
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
	String Status = "0";
	String notifier="";
	String sender="";
	String type="";
	String status="";
	String gid="";
	String gname="";
	String sendername="";
	String nid="";

@Override
public IBinder onBind(Intent intent) {
return null;
}
@Override
public void onCreate() {
	
	new Thread(new Runnable() {
     
     public void run() {
         // TODO Auto-generated method stub
    	 //use to repeat the same process
         while (true) {
             try {
                 Thread.sleep(10000);
                 mHandler.post(new Runnable() {

                    
                     public void run() {
                         // TODO Auto-generated method stub
                     	//Toast.makeText(getBaseContext(),"hello",Toast.LENGTH_LONG).show();
                     	//createNotification();
                     	getValues("http://10.0.2.2:1000/test/tester.php");
                     	
                     }
                 });
             } catch (Exception e) {
                 // TODO: handle exception
             }
             
         }
     }
 }).start();

	
//code to execute when the service is first created
}
@Override
public void onDestroy() {
//code to execute when the service is shutting down
}
@Override
public void onStart(Intent intent, int startid) {
	

}

/** creating a notification which is needed to send to a user. */
public void createNotification() {
	 NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	   // Instantiate the Notification with an icon, ticker text, and when to be displayed
	   int icon = R.drawable.ic_launcher;
	   CharSequence tickerText = "New notification";
	   long when = System.currentTimeMillis(); //now
	   Notification notification = new Notification(icon, tickerText, when);
	   
	   // Define the notification's message and PendingIntent
	   Context context = getApplicationContext();
	   // The activity PostNotification.class will be fired when notification will be opened.
	   Intent notificationIntent = new Intent(this, Notification_handler.class);
	   notificationIntent.putExtra("notifier", notifier);
	   notificationIntent.putExtra("sender", sender);
	   notificationIntent.putExtra("type", type);
	   notificationIntent.putExtra("Status", Status);
	   notificationIntent.putExtra("gid", gid);
	   notificationIntent.putExtra("gname", gname);
	   notificationIntent.putExtra("sendername", sendername);
	   PendingIntent pendingIntent = PendingIntent.getActivity(this, count, notificationIntent, 0);

	   // Makes the notification disappear after clicked in the status bar
	   notification.flags|=Notification.FLAG_AUTO_CANCEL;

	   // When the notification will be fired, notify the user with default notification phone sound.
	   notification.defaults|=Notification.DEFAULT_SOUND;
	   notification.defaults|=Notification.DEFAULT_LIGHTS;

	   notification.setLatestEventInfo(context, sendername, gname, pendingIntent);

	   // Pass the Notification object to NotificationManager
	   notificationManager.notify(count, notification);
	   count++;
	   

}

/** Getting all the details of a particular user. */
public void getValues(String url)
{
	ListView listView;
    myContent = new ArrayList<HashMap<String,Object>>();
    HashMap<String, Object> hm;
    int stat=1;
    
    ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
    //http post
    //convert response to string
    if(stat==1)
    {
    	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
   	 try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            }catch(Exception e){
                Log.e("log_tag", "Error in http connection"+e.toString());
           }

     try{
          BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
           sb = new StringBuilder();
           sb.append(reader.readLine() + "\n");

           String line="0";
           while ((line = reader.readLine()) != null) {
                          sb.append(line + "\n");
            }
            is.close();
            result=sb.toString();
            }
            catch(Exception e){
                  Log.e("log_tag", "Error converting result "+e.toString());
            }
    //paring data
    try{
  	 
          jArray = new JSONArray(result);
          JSONObject json_data=null;
          for(int i=0;i<jArray.length();i++){
                 json_data = jArray.getJSONObject(i);
                 //Status=json_data.getString("status");
                 notifier=json_data.getString("notifier");
                 nid=json_data.getString("id");
             	 sender=json_data.getString("sender");
             	 type=json_data.getString("type");
             	 Status=json_data.getString("status");
             	 gid=json_data.getString("gid");
             	 gname=json_data.getString("gname");
             	 sendername=json_data.getString("sendername");
             	if(Status.equals("1"))
            	{
            		createNotification();
            		deleteNotification("http://10.0.2.2:1000/test/deletenotification.php");
            		Status="0";
            	}
              
             }
          }
          catch(JSONException e1)
          {
        	  Log.e("log_tag", e1.toString());
        	  //Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
          } 
          catch (ParseException e1) 
          {
    			e1.printStackTrace();
    	  }        
    		
    }
}
/** Use to delete a notification which is already received */
    public void deleteNotification(String url)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        //convert response to string
        if(stat==1)
        {
            nameValuePairs.add(new BasicNameValuePair("id",nid));
        	
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }

         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }       
        		
        }
    
}

}
