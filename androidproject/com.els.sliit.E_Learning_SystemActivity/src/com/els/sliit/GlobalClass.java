/*
* globalClass
*
* Version 1.0
*
* Main class of the application.
* Keeps global variables which can be accessed by other activities.
*/
package com.els.sliit;

import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.XMPPConnection;

import com.almondmendoza.drawings.DrawingSurface;

import chat.MessageThread;

import android.app.Application; 

public class GlobalClass extends Application {
	
	private static String userName;
	private static String password;
	private static String filePath;
	private static String shareMessage;
	private static String openfireServerIP;
	private static int openfireServerPort;
	private static String webServerIP;
    public static XMPPConnection m_connection;
    private static List<MessageThread> m_messageThreads;
    private static DrawingSurface drawingSurface;
    private static String currentSessionID;
    
    public static boolean myScreen = true;

	/** Called when the activity is first created. */
	@Override
	public void onCreate() {
	    
		super.onCreate();
	    
	    userName="";
	    password="";
	    filePath="";
	    shareMessage="";
	    openfireServerIP="10.0.2.2";
	    openfireServerPort=5222;
	    webServerIP = "10.0.2.2:1000";
	    XMPPConnection m_connection = null;
	    List<MessageThread> m_messageThreads = new ArrayList<MessageThread>();
	    currentSessionID = "";
	    
	}

	/** Gets the user name of the current user */	
	public static String getUsername() {
	    return userName;
	}
	
	
    
	/** Sets the user name of the current user */
	public static void setUsername(String userName) {
	    GlobalClass.userName = userName;
	}
	
	/** Retrieve the path of the resource which is selected 
	 * from the picture browser and directory browser */
	public static String getFilePath() {
	    return filePath;
	}

	/** Store the path of the resource which is selected 
	 * from the picture browser and directory browser */
	public static void setFilePath(String path) {
	    GlobalClass.filePath = path;
	}
	
	/** Retrieve the message typed by the user when uploading the content */
	public static String getShareMessage() {
	    return shareMessage;
	}

	/** Store the message typed by the user when uploading the content */
	public static void setShareMessage(String message) {
	    GlobalClass.shareMessage = message;
	}
	
	/** Gets the openfireServerIP */	
	public static String getOpenfireServerIP() {
	    return openfireServerIP;
	}
	
	/** Gets the webServerIP */	
	public static String getWebServerIP() {
	    return webServerIP;
	}
	
	/** Gets the openfireServerPort */	
	public static int getOpenfireServerPort() {
	    return openfireServerPort;
	}
	
	/** Sets the XMPP connection */
	public static void setConnection( XMPPConnection newConnection)
	{
		m_connection = newConnection;
	}
	
	/** Gets the XMPP connection */
	public static XMPPConnection setConnection()
	{
		if(m_connection != null)
		{
			return m_connection;
		}
		else
		{
			return null;
		}
	}
	
	/** Sets the Thread */
	
	public static void setMsgThread(List<MessageThread> msgThread)
	{
		if(!msgThread.isEmpty())
		{
			m_messageThreads.clear();
			m_messageThreads.addAll(msgThread);
		}
	}
	
	/** Gets the Thread */
	
	public static List<MessageThread> getThread()
	{
		if(m_messageThreads == null)
			m_messageThreads = new ArrayList<MessageThread>();

			return m_messageThreads;

	}
	/** Clear thread */
	public static void clearThread()
	{
		if(m_messageThreads == null)
			m_messageThreads = new ArrayList<MessageThread>();
		
		m_messageThreads.clear();
	}
	
	/** Preserves the canvas */
	public static void preserveCanvas(DrawingSurface drSurface)
	{
		drawingSurface = drSurface;
	}
	
	/** Preserves the canvas */
	public static DrawingSurface getCanvas()
	{
		return drawingSurface;
	}
	
	/** Sets current session id */
	public static void setCurrentSessionID(String sessionID)
	{
		currentSessionID = sessionID;
	}
	
	/** Gets the current session id */
	public static String getCurrentSession()
	{
		return currentSessionID;
	}

}
