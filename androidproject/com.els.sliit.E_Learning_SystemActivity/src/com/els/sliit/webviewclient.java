//E-Learning Environment
//webviewclient.java 
//Handles activities related to 
//displaying and presenting of uploaded content in
//android webview user defined format
package com.els.sliit;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class webviewclient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
