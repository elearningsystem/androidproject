/*
 * Profile_Update
 *
 * Version 1.0
 *
 */
package com.els.sliit;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.Toast;

public class Profile_Update extends Activity {

	//Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Enable main thread to access network
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// Set xml layout of the profie upate
		setContentView(R.layout.profile_update_fragment);

		// Change the action bar title
		ActionBar acBar = getActionBar();
		acBar.setTitle("Manage Profile");

		// Loads values stored inside the GlobalClass to text boxes
		EditText txtPath = (EditText) findViewById(R.id.txtProPicPath);

		txtPath.setText(GlobalClass.getFilePath());

		// Button leave click event
		Button btnLeave = (Button) findViewById(R.id.btnLeave);

		btnLeave.setOnClickListener(new View.OnClickListener() {

		
			public void onClick(View v) {
				// Calls leaveCommunity method
				leaveCommunity();
			}
		});

		//Button Browse click event
		Button btnBrowse = (Button)findViewById(R.id.btnBrowsePic);

		btnBrowse.setOnClickListener(new View.OnClickListener() {

		
			public void onClick(View v) {

				//Open the Picture Browser
				startActivity(new Intent(Profile_Update.this,PictureBrowser.class));
				finish();//

			}
		});

		//Button Change Picture click event
		Button btnChangePic = (Button)findViewById(R.id.btnChangePic);

		btnChangePic.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				//Get text of the file path text box
				EditText txtPath2 = (EditText) findViewById(R.id.txtProPicPath);

				//Checks the path is empty or not
				if(!txtPath2.getText().toString().isEmpty())
				{

					//Calls file upload method
					doFileUpload(GlobalClass.getFilePath());

					//Calls file download method
					downloadFile("http://10.0.2.2:1000/eLearningWebServices/Profile/profPics/"+GlobalClass.getUsername()+".jpg");

					//Clears the file path
					GlobalClass.setFilePath("");
					txtPath2.setText(GlobalClass.getFilePath());

				}
				else
				{
					Toast.makeText(getApplicationContext(), "Browse a file to upload!", Toast.LENGTH_LONG).show();
				}

			}
		});

		//Change Password button click event
		Button btnChangePassword = (Button)findViewById(R.id.btnChangePassword);

		btnChangePassword.setOnClickListener(new View.OnClickListener() {

	
			public void onClick(View v) {

				changePassword();

			}
		});

	}
	/** Occurs with the onclick event of the leave button.
	 * Getting the password of the particular user and removing him from the system
	 * upon successful validation.
	 */
	public void leaveCommunity()
	{

		AlertDialog.Builder alert = new AlertDialog.Builder(Profile_Update.this);

		alert.setTitle("Leave");
		alert.setMessage("Please Enter Your Password");

		// Set an EditText view to get user input 
		final EditText input = new EditText(Profile_Update.this);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		alert.setView(input);

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				String password = input.getText().toString();
				String userName = GlobalClass.getUsername();

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				//http post
				nameValuePairs.add(new BasicNameValuePair("username",userName));
				nameValuePairs.add(new BasicNameValuePair("password",password));
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Profile/DeleteUser.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is = entity.getContent();
				}catch(Exception e){
					Log.e("log_tag", "Error in http connection"+e.toString());
				}

				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					sb = new StringBuilder();
					sb.append(reader.readLine() + "\n");

					String line="0";
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					is.close();
					result=sb.toString();
				}
				catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}

				//paring data
				String status="";

				try{

					jArray = new JSONArray(result);
					JSONObject json_data=null;
					for(int i=0;i<jArray.length();i++)
					{
						json_data = jArray.getJSONObject(i);
						status=json_data.getString("status");
					}
				}
				catch(JSONException e1)
				{
					Log.e("log_tag", e1.toString());
				} 
				catch (ParseException e1) 
				{
					e1.printStackTrace();
				} 

				if(status.equals("success"))
				{
					startActivity(new Intent(Profile_Update.this,Login.class));
					finish();//
				}
				else
				{
					AlertDialog.Builder alertError = new AlertDialog.Builder(Profile_Update.this);
					alertError.setTitle("Error");
					alertError.setMessage("Invalid Password");
					alertError.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int whichButton) {
							// Canceled.
						}
					});

					alertError.show();

				}
			}

		});

		alert.show();
	}

	/** Use to change the password of an existing user. */
	void changePassword()
	{

		EditText txtOldPassword = (EditText)findViewById(R.id.txtOldPassword);
		EditText txtNewPassword = (EditText)findViewById(R.id.txtNewPassword);

		String oldPassword = txtOldPassword.getText().toString();
		String newPassword = txtNewPassword.getText().toString();

		if(oldPassword.isEmpty() && newPassword.isEmpty())
		{
			Toast.makeText(getApplicationContext(), "Password fields are empty!", Toast.LENGTH_SHORT).show();
		}
		else
		{
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername()));
			nameValuePairs.add(new BasicNameValuePair("oldpassword",oldPassword));
			nameValuePairs.add(new BasicNameValuePair("newpassword",newPassword));

			//http post
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Profile/PasswordChange.php");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
			}catch(Exception e){
				Log.e("log_tag", "Error in http connection"+e.toString());
			}
			//convert response to string

			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				sb = new StringBuilder();
				sb.append(reader.readLine() + "\n");

				String line="0";
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result=sb.toString();

			}catch(Exception e){
				Log.e("log_tag", "Error converting result "+e.toString());
			}
			//paring data
			String status=null;

			try{

				jArray = new JSONArray(result);
				JSONObject json_data=null;
				for(int i=0;i<jArray.length();i++){
					json_data = jArray.getJSONObject(i);
					status=json_data.getString("status");

				}
				if(!(status.equals("error")))
				{
					Toast.makeText(getBaseContext(), "Password successfully changed" ,Toast.LENGTH_LONG).show();
					clearPwdFields();
				}
				else
				{
					Toast.makeText(getBaseContext(), "Old password is incorrect" ,Toast.LENGTH_LONG).show();
				}
			}
			catch(JSONException e1){
				Toast.makeText(getBaseContext(), "Error" ,Toast.LENGTH_LONG).show();
				Log.e("log_tag", e1.toString());


			} catch (ParseException e1) {
				e1.printStackTrace();

			}
		}
	}

	void clearPwdFields()
	{
		EditText txtOldPassword = (EditText)findViewById(R.id.txtOldPassword);
		EditText txtNewPassword = (EditText)findViewById(R.id.txtNewPassword);

		txtOldPassword.setText("");
		txtNewPassword.setText("");
	}

	//Stores the profile picture
	Bitmap bmImg;

	/** Download the profile picture from the web server */
	void downloadFile(String fileUrl){
		URL myFileUrl =null; 
		try {
			myFileUrl= new URL(fileUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn= (HttpURLConnection)myFileUrl.openConnection();
			conn.setDoInput(true);
			conn.connect();
			int length = conn.getContentLength();
			int[] bitmapData =new int[length];
			byte[] bitmapData2 =new byte[length];
			InputStream is = conn.getInputStream();

			bmImg = BitmapFactory.decodeStream(is);
			//imView.setImageBitmap(bmImg);

			QuickContactBadge badgeSmall = (QuickContactBadge) findViewById(R.id.profPictute); 

			badgeSmall.setImageBitmap(bmImg);
			Toast.makeText(getBaseContext(), "Picture changed" ,Toast.LENGTH_LONG).show();

		} catch (IOException e) {

			Log.e("log_tag", e.getMessage().toString());

		}


	}

	/** Upload the new profile picture the web server */
	private void doFileUpload(String filePath){

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String existingFileName = filePath;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;
		String responseFromServer = "";

		String userName = GlobalClass.getUsername();

		String urlString = "http://10.0.2.2:1000/eLearningWebServices/Profile/UploadProfPic.php?userName="+userName;

		try
		{
			//------------------ CLIENT REQUEST
			FileInputStream fileInputStream = new FileInputStream(new File(existingFileName) );
			// open a URL connection to the Servlet
			URL url = new URL(urlString);
			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();
			// Allow Inputs
			conn.setDoInput(true);
			// Allow Outputs
			conn.setDoOutput(true);
			// Don't use a cached copy.
			conn.setUseCaches(false);

			// Use a post method.
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
			dos = new DataOutputStream( conn.getOutputStream() );
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + existingFileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			// create a buffer of maximum size
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0)
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// close streams
			Log.e("Debug","File is written");

			Toast toast = Toast.makeText(getBaseContext(), "File is Uploaded Sucessfully", Toast.LENGTH_LONG);
			toast.show();

			fileInputStream.close();
			dos.flush();
			dos.close();

		}
		catch (MalformedURLException ex)
		{
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		catch (IOException ioe)
		{
			Log.e("Debug", "error: " + ioe.getMessage(), ioe);
		}

		//------------------ read the SERVER RESPONSE
		try {
			inStream = new DataInputStream ( conn.getInputStream() );
			String str;

			while (( str = inStream.readLine()) != null)
			{
				Log.e("Debug","Server Response "+str);
			}
			inStream.close();

		}
		catch (IOException ioex){
			Log.e("Debug", "error: " + ioex.getMessage(), ioex);
		}

	}

	/** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Dashboard");
		}

		MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(Profile_Update.this,Dashboard.class));
			this.finish();//
		}

		return true;
		case 1:
			GlobalClass.setUsername("");
			startActivity(new Intent(Profile_Update.this,Login.class));
			this.finish();//

			return true;

		}
		return false;
	}


}
