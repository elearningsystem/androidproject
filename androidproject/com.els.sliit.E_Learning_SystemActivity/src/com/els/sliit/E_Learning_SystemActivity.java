/*
 * E_Learning_SystemActivity
 *
 * Version 1.0
 *
 * Main activity which starts when the application is first launched.
 *  
 */
package com.els.sliit;

import com.almondmendoza.drawings.DrawingActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;

public class E_Learning_SystemActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// Go to Login
		//startActivity(new Intent(E_Learning_SystemActivity.this, Login.class));
		
		startActivity(new Intent(E_Learning_SystemActivity.this, Login.class));
		this.finish();//
		
      //  Intent drawIntent = new Intent(E_Learning_SystemActivity.this, DrawingActivity.class);
      //  startActivity( drawIntent);
  

	}

}