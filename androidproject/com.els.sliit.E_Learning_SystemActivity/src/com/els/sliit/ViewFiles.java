//E-Learning Environment
//ViewFiles.java 
//Handles activities related to 
//displaying and presenting of uploaded content
package com.els.sliit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class ViewFiles extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewfiles);
        
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        //storing data passed by previous intent
        Bundle extras=getIntent().getExtras();
	    String filename=extras.getString("docName");
	    
	    String url = "http://docs.google.com/viewer?url=http://dl.dropbox.com/u/92201062/"+filename;
        
        WebView wvbrowser;
        wvbrowser=(WebView)findViewById(R.id.webView1);
        wvbrowser.setWebViewClient(new webviewclient());
        
        //enabling javascript in integrated webview 
        wvbrowser.getSettings().setJavaScriptEnabled(true);
        wvbrowser.loadUrl(url);
        
        //Back button click event to go to the share view intent
        Button btnBack = (Button) findViewById(R.id.btnBack);
        
        btnBack.setOnClickListener(new View.OnClickListener() {
 			
 		
 			public void onClick(View v) {
 				
 				//Go to Share View
 				startActivity(new Intent(ViewFiles.this,ShareView.class));
 				finish();//
 				
 			}
 		});
        
    }
    
    /** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Dashboard");
		}

		MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Execute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(ViewFiles.this,Dashboard.class));
			this.finish();//
		}

		return true;
		case 1:
			GlobalClass.setUsername("");
			startActivity(new Intent(ViewFiles.this,Login.class));
			this.finish();//

			return true;

		}
		return false;
	}
}
