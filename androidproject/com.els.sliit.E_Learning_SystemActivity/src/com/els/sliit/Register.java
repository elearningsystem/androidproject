/*
* Register
*
* Version 1.0
*
*/
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends Activity {
	
	//Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
	
	//Openfire server 
	
	private String SERVER_HOST = "";
	private int SERVER_PORT = 0;
	private String SERVICE_NAME = "";	
	private String WEB_SERVER_IP ="";

	private XMPPConnection m_connection;
	private Handler m_handler;

	/** Called when the activity is first created. */
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_fragment);
        
        ActionBar acBar = getActionBar();
        acBar.setTitle("Register");
        
        //set settings
        SERVER_PORT = 5222;
        SERVICE_NAME = getResources().getString(R.string.OPENFIRE_SERVER_IP);
        WEB_SERVER_IP = getResources().getString(R.string.WEB_SERVER_IP);
        SERVER_HOST = getResources().getString(R.string.OPENFIRE_SERVER_IP);
        
        // Allow main thread to connect to networks
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        
        // Back button click event
		Button btnBack = (Button)findViewById(R.id.btnBack);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				
				 //Go to login
				 startActivity(new Intent(Register.this,Login.class));
				 finish();//
			}
		});
		
		Button btnRegister = (Button) findViewById(R.id.btnRegister);
		
		btnRegister.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				EditText txtUserName = (EditText)findViewById(R.id.txtUserName);
				EditText txtPassword = (EditText)findViewById(R.id.txtPassword);
				EditText txtRetypePassword = (EditText)findViewById(R.id.txtReTypePassword);
				EditText txtName = (EditText)findViewById(R.id.txtName);
				String userName = txtUserName.getText().toString();
				String password = txtPassword.getText().toString();
				String reTypePassword = txtRetypePassword.getText().toString();
				String name = txtName.getText().toString();
				
				// openfire server connectivity
				ConnectionConfiguration config = new ConnectionConfiguration(
						SERVER_HOST, SERVER_PORT, SERVICE_NAME);
				m_connection = new XMPPConnection(config);

				m_handler = new Handler();
				try {
					m_connection.connect();
				} catch (XMPPException e) {
					e.printStackTrace();
				}

				if(userName.isEmpty() || password.isEmpty() || reTypePassword.isEmpty() || name.isEmpty())
				{
					Toast.makeText(getBaseContext(), "All fields are required!" ,Toast.LENGTH_LONG).show();
				}
				else
				{
					if(!password.equals(reTypePassword))
					{
						Toast.makeText(getBaseContext(), "Passwords are different!" ,Toast.LENGTH_LONG).show();					
					}
					else
					{
						GlobalClass.setUsername(userName);
						
						ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("name",name));
				        nameValuePairs.add(new BasicNameValuePair("userName",userName));
				        nameValuePairs.add(new BasicNameValuePair("password",password));
				        
				        //register with the openfire server
				        AccountManager acountMngr = new AccountManager(m_connection);
				        Map<String, String> attributes = new HashMap<String, String>();
				        attributes.put("username",userName);
				        attributes.put("password",password);
				        attributes.put("email", "abc@gmail.com");
				        attributes.put("name", name);
				        
				        try {
				        	acountMngr.createAccount(userName, password, attributes);

				        } catch (XMPPException e) {
				              Toast.makeText(getApplicationContext(),"Username Already Exists",Toast.LENGTH_SHORT).show();
				            Log.e("openfire",e.getMessage());
				        } 
				        
				    
				        
				        //http post
				        try{
				             HttpClient httpclient = new DefaultHttpClient();
				             HttpPost httppost = new HttpPost("http://"+WEB_SERVER_IP+"/eLearningWebServices/Profile/Register.php");
				             httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				             HttpResponse response = httpclient.execute(httppost);
				             HttpEntity entity = response.getEntity();
				             is = entity.getContent();
				             }catch(Exception e){
				                 Log.e("log_tag", "Error in http connection"+e.toString());
				            }
				        //convert response to string
				       
				        try{
				              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				               sb = new StringBuilder();
				               sb.append(reader.readLine() + "\n");

				               String line="0";
				               while ((line = reader.readLine()) != null) {
				                              sb.append(line + "\n");
				                }
				                is.close();
				                result=sb.toString();
				                //Log.e("log_tag", result.toString());
				                }catch(Exception e){
				                      Log.e("log_tag", "Error converting result "+e.toString());
				                }
				        //paring data
				        String status=null;
				      
				        try{
				      	 
				              jArray = new JSONArray(result);
				              JSONObject json_data=null;
				              for(int i=0;i<jArray.length();i++){
				                     json_data = jArray.getJSONObject(i);
				                     status=json_data.getString("status");
				                    
				                     }
				              if(!(status.equals("Error")))
				              {
				              startActivity(new Intent(Register.this,Dashboard.class));
				              finish();//
				              }
				              
				              else
				              {
				            	  Toast.makeText(getBaseContext(), "User Name Already Exists!" ,Toast.LENGTH_LONG).show();
				              }
				              }
				              catch(JSONException e1){
				            	 Toast.makeText(getBaseContext(), "Error" ,Toast.LENGTH_LONG).show();
				            	  Log.e("log_tag", e1.toString());
				            	  
				            	   
				              } catch (ParseException e1) {
				        			e1.printStackTrace();
				            	    
				        	} 
					}
				}

			}
		});
		
	}
	
	

}
