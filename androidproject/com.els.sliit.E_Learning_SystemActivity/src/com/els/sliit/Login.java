/*
 * Login
 *
 * Version 1.0
 *
 * Authenticate the user before entering to the application by
 * validating the username and password.
 *
 */
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jivesoftware.smack.XMPPConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.almondmendoza.drawings.PlayBackRecord;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {

	//Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
	TextView tv;
	

	

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_fragment);

		ActionBar acBar = getActionBar();
		acBar.setTitle("Login");

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// Register Button Click Event
		Button btnRegister = (Button) findViewById(R.id.btnRegister);

		btnRegister.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				//Go to Register
				//startActivity(new Intent(Login.this,Register.class));			
				
				startActivity(new Intent(Login.this,Register.class));
				finish();//
				
			}
		});

		// Login Button Click Event
		Button btnLogin = (Button) findViewById(R.id.btnLogin);

		btnLogin.setOnClickListener(new View.OnClickListener() {

			
			public void onClick(View v) {

				Intent myIntent = new Intent(getApplicationContext(), ServiceTemplate.class);
		        myIntent.putExtra("extraData", "somedata");
		        startService(myIntent);
				//Go to Dashboard
				EditText username = (EditText)findViewById(R.id.txtBoxUserName);
				EditText password = (EditText)findViewById(R.id.txtBoxPassword);
				String txtusername = username.getText().toString();
				String txtpassword = password.getText().toString();
				GlobalClass.setUsername(txtusername);
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("username",txtusername));
				nameValuePairs.add(new BasicNameValuePair("password",txtpassword));
				//http post
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://10.0.2.2:1000/eLearningWebServices/Profile/Authentication.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is = entity.getContent();
				}catch(Exception e){
					Log.e("log_tag", "Error in http connection"+e.toString());
				}
				//convert response to string

				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					sb = new StringBuilder();
					sb.append(reader.readLine() + "\n");

					String line="0";
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					is.close();
					result=sb.toString();
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				//paring data
				String dbusername=null;
				String dbpassword;
				try{

					jArray = new JSONArray(result);
					JSONObject json_data=null;
					for(int i=0;i<jArray.length();i++){
						json_data = jArray.getJSONObject(i);
						dbusername=json_data.getString("username");
						dbpassword=json_data.getString("password");

					}
					if(!(dbusername.equals("error")))
					{
						GlobalClass.setUsername(txtusername);
						startActivity(new Intent(Login.this,Dashboard.class));
						finish();//
					
					}
					else
					{
						Toast.makeText(getBaseContext(), "Invalid Attempt, Try Again" ,Toast.LENGTH_LONG).show();
					}
				}
				catch(JSONException e1){
					Toast.makeText(getBaseContext(), "Error" ,Toast.LENGTH_LONG).show();
					Log.e("log_tag", e1.toString());


				} catch (ParseException e1) {
					e1.printStackTrace();

				}

			}
		});

	}


}
