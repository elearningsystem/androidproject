//E-Learning Environment
//viewmembers.java 
//Handles activities related to 
//controlling group activities
package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class viewmembers extends Activity {
	String groupID="";
	String groupName="";
	String groupStatus="";
	private ArrayList <HashMap<String, Object>> myContent;
	private static final String NAME = "name";
	private static final String UNAME = "uname";
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb = null;
	
	/** Sending a notification as a response. */
	public void onCreate(Bundle savedInstanceState) {
   	 super.onCreate(savedInstanceState);
        setContentView(R.layout.viewsessionusers);
        groupID = getIntent().getExtras().getString("gid");
        groupName = getIntent().getExtras().getString("gname");
        groupStatus = getIntent().getExtras().getString("mode");
        
        TextView title = (TextView)findViewById(R.id.tvView);
        title.setText(groupName);
        
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        GetMembers("http://10.0.2.2:1000/test/viewMembers.php",1,"");
        
        Button btnBack = (Button)findViewById(R.id.btnviewuserback);
        btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent notificationIntent = new Intent(viewmembers.this,deletesessions.class);
				   startActivity(notificationIntent);
				   finish();//
			}
		});

}
	/** Getting the all subscribed members of a particular group. */
	public void GetMembers(String url,int status,String para)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=status;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        try{
             HttpClient httpclient = new DefaultHttpClient();
             HttpPost httppost = new HttpPost(url);
             httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
             HttpResponse response = httpclient.execute(httppost);
             HttpEntity entity = response.getEntity();
             is = entity.getContent();
             }catch(Exception e){
                 Log.e("log_tag", "Error in http connection"+e.toString());
            }
        //convert response to string
        if(stat==1)
        {
        	nameValuePairs.add(new BasicNameValuePair("gid",groupID));
        	nameValuePairs.add(new BasicNameValuePair("username",GlobalClass.getUsername().toString()));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	listView = (ListView)findViewById(R.id.lvView);
        	listView.setClickable(true);
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        String name;
        String uName;
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     name=json_data.getString("name");
                     uName=json_data.getString("username");
                     hm = new HashMap<String, Object>();
                     hm.put(NAME, name);
                     hm.put(UNAME, uName);
                     myContent.add(hm);
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        listView.setAdapter(new myListAdapter(myContent,this,1));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
	
	/** Adding received data to a holder to display in controls. */ 
    private class myListAdapter extends BaseAdapter{
    	
    	
    	private ArrayList<HashMap<String, Object>> Content; 
    	private LayoutInflater mInflater;
    	int status;
    	
    	
		public myListAdapter(ArrayList<HashMap<String, Object>> content, Context context,int stat){
			status=stat;
			Content = content;
			mInflater = LayoutInflater.from(context);
		}
    	
    	
    	public int getCount() {
			// TODO Auto-generated method stub
			return Content.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return Content.get(position);
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			if(status==1)
			{
			// TODO Auto-generated method st
			// A ViewHolder keeps references to children views to avoid unneccessary calls
            // to findViewById() on each row.
			ViewHolder holder;
			
			// When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null
			
			 if (convertView == null) {
	             convertView = mInflater.inflate(R.layout.viewformat, null);
	             // Creates a ViewHolder and store references to the two children views
	             // we want to bind data to.
	             
	             holder = new ViewHolder();
	             holder.name = (TextView) convertView.findViewById(R.id.tvViewName);
	             holder.uName = (TextView) convertView.findViewById(R.id.tvViewUsername);
	           
	             convertView.setTag(holder);
	                
			 }else {
				 // Get the ViewHolder back to get fast access to the views
				 holder = (ViewHolder) convertView.getTag(); 
			 }
			 	// Bind the data with the holder.
			 
				holder.name.setText((String) Content.get(position).get(NAME));
				
				holder.uName.setText((String) Content.get(position).get(UNAME));
				
				convertView.setOnClickListener(new OnClickListener(){
					
			        public void onClick(View v) {	
			        	
			        	
			        }
			        
			        
			    });
				  
	             Button remove = (Button)convertView.findViewById(R.id.btnViewRemove);
					if(groupStatus.equals("Leave"))
						remove.setVisibility(View.INVISIBLE);
		          	TextView uname = (TextView)convertView.findViewById(R.id.tvViewUsername);
		        	final String strUName = uname.getText().toString();
		        	remove.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							operation("Remove",1,groupID,strUName);
				        	GetMembers("http://10.0.2.2:1000/test/viewMembers.php",1,"");
						}
					});
				//Toast.makeText(getBaseContext(), groupStatus ,Toast.LENGTH_LONG).show();
				
				
			}
			return convertView;
		}
		class ViewHolder {
			TextView name;
	 	    TextView uName;
	 	    
	 }
 }

/** Use to leave or remove a group depends on the user is the creator of the group or not. */
public void operation(String action,int type,String id,String uname)
{
 final int status = type;
 final String fAction=action;
 final String gID = id;
 final String user=uname;
	AlertDialog.Builder alert = new AlertDialog.Builder(viewmembers.this);

	alert.setTitle(action);
	alert.setMessage("You are about to "+action+" the member.");			
	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		public void onClick(DialogInterface dialog, int whichButton) {
			// Canceled.
		}
	});

	alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		public void onClick(DialogInterface dialog, int whichButton) {
			if(status==1)
			{
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				//http post
				nameValuePairs.add(new BasicNameValuePair("gid",gID));
				nameValuePairs.add(new BasicNameValuePair("username",user));
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://10.0.2.2:1000/test/deleteSMember.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is = entity.getContent();
					Toast.makeText(getBaseContext(), "Successfully "+fAction ,Toast.LENGTH_LONG).show();
					GetMembers("http://10.0.2.2:1000/test/viewMembers.php",1,"");
				}catch(Exception e){
					Toast.makeText(getBaseContext(), "Error Occured" ,Toast.LENGTH_LONG).show();
					Log.e("log_tag", "Error in http connection"+e.toString());
					
				}
			}
			
				
		}

	});

	alert.show();
}

/** Menu item click event */
@Override
public boolean onOptionsItemSelected(MenuItem item)
{
	return MenuChoice(item);
}

/** Excute tasks for click events */
private boolean MenuChoice(MenuItem item)
{
	switch (item.getItemId()) {
	case 0:
	{
		startActivity(new Intent(viewmembers.this,Dashboard.class));
		this.finish();//
	}

	return true;
	case 1:
		GlobalClass.setUsername("");
		startActivity(new Intent(viewmembers.this,Login.class));
		this.finish();//

		return true;

	}
	return false;
}
}
