/*
 * SessionActivity
 *
 * Version 1.0
 * E-Learning Environment
 * SessionActivity.java 
 * Handles activities related to adding of groups
 */

package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SessionsActivity extends Activity {
	/** Called when the activity is first created. */
	// Use to handle data passing to the application as json encoded data.
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addgroups);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// create Button click event
		Button btnCreate = (Button) findViewById(R.id.btnCreate);
		Button btnDelete = (Button) findViewById(R.id.btnDelete);

		// delete button click event
		btnDelete.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				//load activity delete_groups
				startActivity(new Intent(SessionsActivity.this,
						DeleteGroups.class));
				finish();//

			}
		});

		btnCreate.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				// Call CreateGroup method
				EditText groupName = (EditText) findViewById(R.id.txtBoxGroupName);
				EditText description = (EditText) findViewById(R.id.txtBoxDescription);
				String gName = groupName.getText().toString();
				String gDescription = description.getText().toString();
				CreateGroup(gName, gDescription);

			}
		});
	}

	// Creating the group
	public void CreateGroup(String gName, String gDescription) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", GlobalClass.getUsername().toString()));
		nameValuePairs.add(new BasicNameValuePair("group_name", gName));
		nameValuePairs.add(new BasicNameValuePair("description", gDescription));
		nameValuePairs.add(new BasicNameValuePair("type", "public"));
		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://10.0.2.2:1000/test/addGroups.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		// paring data
		String status = null;
		try {

			jArray = new JSONArray(result);
			JSONObject json_data = null;
			for (int i = 0; i < jArray.length(); i++) {
				json_data = jArray.getJSONObject(i);
				status = json_data.getString("status");

			}
			if ((status.equals("Error")))
				Toast.makeText(getBaseContext(), "Cannot create the group",
						Toast.LENGTH_LONG).show();

			else {
				Toast.makeText(getBaseContext(), "Group created successfully",
						Toast.LENGTH_LONG).show();
			}
		} catch (JSONException e1) {
			Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
			Log.e("log_tag", e1.toString());

		} catch (ParseException e1) {
			e1.printStackTrace();

		}
	}
	
	/** Add dashboard menu item to action bar. */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		CreateMenu(menu);
		return true;
	}

	/** Create the menu */
	private void CreateMenu(Menu menu)
	{
		MenuItem mnu1 = menu.add(0, 0, 0, "Dashboard");
		{

			mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu1.setIcon(R.drawable.ic_menu_largetiles);
			mnu1.setTitle("Dashboard");
		}

		MenuItem mnu2 = menu.add(0, 1, 1, "Logout");
		{
			mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
					MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			mnu2.setIcon(R.drawable.ic_menu_exit);
			mnu2.setTitle("Logout");
		}

	}

	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(SessionsActivity.this,Dashboard.class));
			this.finish();//
		}

		return true;
		case 1:
			GlobalClass.setUsername("");
			startActivity(new Intent(SessionsActivity.this,Login.class));
			this.finish();//

			return true;

		}
		return false;
	}

}
