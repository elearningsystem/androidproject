/*
 * ImageAdapter 
 *
 * Version 1.0
 *
 * Handle groups gridview with using profile pictures
 *
 */

package com.els.sliit;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {

	private Context context;
	private final String[] NameVal;
	private final String[] val1;
	private Bitmap bmImg;
 
	public ImageAdapter(Context context, String[] NameVal,String[] Unames) {
		this.context = context;
		this.NameVal = NameVal;
		this.val1 = Unames;
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
 
			gridView = new View(context);
 
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.groupitem, null);
 
			// set value into textview
			TextView textView = (TextView) gridView
					.findViewById(R.id.grid_item_label);
			textView.setText(NameVal[position]);
			
			TextView TextUser = (TextView) gridView.findViewById(R.id.Uname);
			TextUser.setText(val1[position]);
 
			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.grid_item_image);
 
			//String mobile = mobileValues[position];
 
			imageView.setImageBitmap(downloadFile("http://10.0.2.2:1000/eLearningWebServices/Profile/profPics/"+val1[position]+".jpg"));
			/*if (mobile.equals("Windows")) {
				imageView.setImageResource(R.drawable.ic_launcher);
			} else if (mobile.equals("iOS")) {
				imageView.setImageResource(R.drawable.ic_launcher);
			} else if (mobile.equals("Blackberry")) {
				imageView.setImageResource(R.drawable.ic_launcher);
			} else {
				imageView.setImageResource(R.drawable.ic_launcher);
			}*/
 
		} else {
			gridView = (View) convertView;
		}
 
		return gridView;
	}
 
	
	public int getCount() {
		return NameVal.length;
	}
 
	
	public Object getItem(int position) {
		return null;
	}
 
	
	public long getItemId(int position) {
		return 0;
	}
	
	public Bitmap downloadFile(String fileUrl){
		URL myFileUrl =null; 
		try {
			myFileUrl= new URL(fileUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn= (HttpURLConnection)myFileUrl.openConnection();
			conn.setDoInput(true);
			conn.connect();
			int length = conn.getContentLength();
			int[] bitmapData =new int[length];
			byte[] bitmapData2 =new byte[length];
			InputStream is = conn.getInputStream();

			bmImg = BitmapFactory.decodeStream(is);

		} catch (IOException e) {

			Log.e("log_tag", e.getMessage().toString());

		}
		return bmImg;


	}

}
