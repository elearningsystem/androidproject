package com.els.sliit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

//E-Learning Environment
//Notification_handler.java 
//Handles activities related to 
//post actions after receiving a notification 
public class Notification_handler extends Activity {
	
private ArrayList <HashMap<String, Object>> myContent;
	

	Handler mHandler = new Handler();
	int count = 0;
	JSONArray jArray;
	String result = null;
	InputStream is = null;
	StringBuilder sb=null;
	String notifier;
	String sender;
	String type;
	String status;
	String gid;
	String gname;
	String nname;
	String sendername;
	String notiname;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
	   	 super.onCreate(savedInstanceState);
	        setContentView(R.layout.notification_handler);
	        
	        notifier = getIntent().getExtras().getString("notifier");
	        sender = getIntent().getExtras().getString("sender");
	        type = getIntent().getExtras().getString("type");
	        status = getIntent().getExtras().getString("Status");
	        gid = getIntent().getExtras().getString("gid");
	        gname = getIntent().getExtras().getString("gname");
	        sendername = getIntent().getExtras().getString("sendername");
	        
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	        
	        Button btnBack = (Button)findViewById(R.id.btnnhback);
	        btnBack.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent notificationIntent = new Intent(Notification_handler.this,FinalNotiActivity.class);
					   startActivity(notificationIntent);
					   finish();//
				}
			});
	        
	        
	        nname=notifier;
	        //starts if the request is a user request
	        if(type.equals("ureq"))
	        {
	        AlertDialog.Builder alert = new AlertDialog.Builder(Notification_handler.this);

			alert.setTitle("Notification");
			String msg = sendername+" wants to join to your group "+gname+".";
			alert.setMessage(msg);			
			alert.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
					ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					//http post
					nameValuePairs.add(new BasicNameValuePair("gid",gid));
					nameValuePairs.add(new BasicNameValuePair("username",sender));
					try{
						HttpClient httpclient = new DefaultHttpClient();
						HttpPost httppost = new HttpPost("http://10.0.2.2:1000/test/deleteSMember.php");
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						HttpResponse response = httpclient.execute(httppost);
						HttpEntity entity = response.getEntity();
						is = entity.getContent();
						
					}catch(Exception e){
						Toast.makeText(getBaseContext(), "Error Occured" ,Toast.LENGTH_LONG).show();
						Log.e("log_tag", "Error in http connection"+e.toString());
						
					}
				}
			});

			alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					UpDateMember("http://10.0.2.2:1000/test/updatemember.php",sender,gid);
					GetName("http://10.0.2.2:1000/test/getCUserName.php",notifier);
					SendNotification(sender,notifier,"aureq","1",gid,gname,notiname);
				
				}
	});
			alert.show();
	        }
	        
	        //starts if the request is a creator request
	        else if(type.equals("creq"))
	        {
	        AlertDialog.Builder alert = new AlertDialog.Builder(Notification_handler.this);

			alert.setTitle("Notification");
			String msg = sender+" sent a request you to join "+gname+" group.";
			alert.setMessage(msg);			
			alert.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
				}
			});

			alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					UpDateMember("http://10.0.2.2:1000/test/updatemember.php",notifier,gid);
					GetName("http://10.0.2.2:1000/test/getCUserName.php",notifier);
					SendNotification(sender,notifier,"acreq","1",gid,gname,notiname);
				
				}
	});
			alert.show();
	        }
	        
	        else if(type.equals("acreq"))
	        {
	        AlertDialog.Builder alert = new AlertDialog.Builder(Notification_handler.this);

			alert.setTitle("Notification");
			String msg = sender+" joined to your group "+gname+" group.";
			alert.setMessage(msg);			
			alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
				}
			});

			alert.show();
	        }
	        //starts is the request is an accepted user request
	        else if(type.equals("aureq"))
	        {
	        AlertDialog.Builder alert = new AlertDialog.Builder(Notification_handler.this);

			alert.setTitle("Notification");
			String msg = sender+" added you to the group "+gname+"";
			alert.setMessage(msg);			
			alert.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
				}
			});

		
			alert.show();
	        }
	        else
	        {
	        AlertDialog.Builder alert = new AlertDialog.Builder(Notification_handler.this);

			alert.setTitle("Notification");
			String msg = sender+" sent you a message through the group '"+gname+"' : "+type;
			alert.setMessage(msg);			
			alert.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int whichButton) {
					// Canceled.
				}
			});

			alert.show();
	        }

}
	/** If the request is accepted, changing the user staus from 'pending' to 'accepted. */
	public void UpDateMember(String url,String username,String groupId)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        //convert response to string
        if(stat==1)
        {
            nameValuePairs.add(new BasicNameValuePair("groupID",groupId));
            nameValuePairs.add(new BasicNameValuePair("username",username));
        	
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }

         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }       
        		
        }
    }
	
	/** Sending a notification as a response. */
	public void SendNotification(String notifier,String sender,String type,String status,String gid,String gname,String sendername)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
    
      
        //convert response to string
        if(stat==1)
        {
            nameValuePairs.add(new BasicNameValuePair("notifier",notifier));
        	nameValuePairs.add(new BasicNameValuePair("sender",sender));
        	nameValuePairs.add(new BasicNameValuePair("type",type));
        	nameValuePairs.add(new BasicNameValuePair("status",status));
        	nameValuePairs.add(new BasicNameValuePair("gid",gid));
        	nameValuePairs.add(new BasicNameValuePair("gname",gname));
        	nameValuePairs.add(new BasicNameValuePair("sendername",sendername));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://10.0.2.2:1000/test/addNotification.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
        	
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }      
        }
    }
	
	/** Getting a full name of a particular user to mark as the sender of the notification. */
	public void GetName(String url,String uname)
    {
    	ListView listView;
        myContent = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> hm;
        int stat=1;
        
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        //http post
        //convert response to string
        if(stat==1)
        {
        	nameValuePairs.add(new BasicNameValuePair("username",uname));
       	 try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                }catch(Exception e){
                    Log.e("log_tag", "Error in http connection"+e.toString());
               }
    
         try{
              BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
               sb = new StringBuilder();
               sb.append(reader.readLine() + "\n");

               String line="0";
               while ((line = reader.readLine()) != null) {
                              sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
                }
                catch(Exception e){
                      Log.e("log_tag", "Error converting result "+e.toString());
                }
        //paring data
        try{
      	 
              jArray = new JSONArray(result);
              JSONObject json_data=null;
              for(int i=0;i<jArray.length();i++){
                     json_data = jArray.getJSONObject(i);
                     notiname=json_data.getString("name");
                  
                 }
              }
              catch(JSONException e1)
              {
            	  Log.e("log_tag", e1.toString());
            	  Toast.makeText(getBaseContext(), "No Content Available" ,Toast.LENGTH_LONG).show();
              } 
              catch (ParseException e1) 
              {
        			e1.printStackTrace();
        	  }        
        		
        }
    }
	
	/** Menu item click event */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return MenuChoice(item);
	}

	/** Excute tasks for click events */
	private boolean MenuChoice(MenuItem item)
	{
		switch (item.getItemId()) {
		case 0:
		{
			startActivity(new Intent(Notification_handler.this,Dashboard.class));
			this.finish();//
		}

		return true;
		case 1:
			GlobalClass.setUsername("");
			startActivity(new Intent(Notification_handler.this,Login.class));
			this.finish();//
			return true;

		}
		return false;
	}
	
}
