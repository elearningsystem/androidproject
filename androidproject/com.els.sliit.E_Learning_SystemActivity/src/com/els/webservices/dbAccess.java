/*
 * dbAccess
 *
 * Version 1.0
 *
 * Handles data base connectivity
 */

package com.els.webservices;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.ParseException;
import android.util.Log;
import android.widget.Toast;

import com.els.sliit.GlobalClass;
import com.els.sliit.WebConnectivity;

public class dbAccess {
	
	WebConnectivity webConn;
	JSONArray jArray;
	
	/**
	 * Gets group members for given group ID
	 */
	public CharSequence[] getPeopleForSession(String groupID)
	{
		
		webConn = new WebConnectivity();
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("groupID", groupID));
		
		// Send request to the web server
		InputStream iStream = webConn.httpPost("eLearningWebServices/Chat/GetGrpMembers.php",
				nameValuePairs);
		
		String stringResponse = webConn.convertResponse(iStream);

		String value = "";

		//Convert to chat sequence
		CharSequence[] nameList = null;

		int size = 1;
		
		try {

			jArray = new JSONArray(stringResponse);
			JSONObject json_data = null;

			size = jArray.length()+1;

			nameList = new CharSequence[size];

			for (int i = 0; i < size-1; i++) {
				if(i == 0)
				{
				json_data = jArray.getJSONObject(0);
				value = json_data.getString("creator");
				nameList[0] = value;
				}

				json_data = jArray.getJSONObject(i);
			    value = json_data.getString("username");
			
				nameList[i+1] = value;
			}
		} catch (JSONException e1) {
			Log.e("log_tag", e1.toString());
			return null;

		} catch (ParseException e1) {
			e1.printStackTrace();
			return null;

		}
		
		
		return nameList;
	}

}
