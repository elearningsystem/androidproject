package com.els.webservices;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.json.JSONArray;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.QuickContactBadge;
import android.widget.Toast;

import com.els.sliit.GlobalClass;
import com.els.sliit.R;

/**
 * Will upload the provides file to the destination folder in the web server
 */
public class FileTransfer {
	

	
	public void uploadFile(String filePath,String destinationPath){
		
		//Use to handle data passing to the application as json encoded data.
		JSONArray jArray;
		String result = null;
		InputStream is = null;
		StringBuilder sb=null;
		
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String existingFileName = filePath;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;
		String responseFromServer = "";

		String userName = GlobalClass.getUsername();

		String urlString = "http://10.0.2.2:1000/eLearningWebServices/Common/FileTransfer.php?userName="+userName+"&destinationPath="+destinationPath;

		try
		{
			//------------------ CLIENT REQUEST
			FileInputStream fileInputStream = new FileInputStream(new File(existingFileName) );
			// open a URL connection to the Servlet
			URL url = new URL(urlString);
			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();
			// Allow Inputs
			conn.setDoInput(true);
			// Allow Outputs
			conn.setDoOutput(true);
			// Don't use a cached copy.
			conn.setUseCaches(false);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
			dos = new DataOutputStream( conn.getOutputStream() );
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + existingFileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			// create a buffer of maximum size
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0)
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);



			fileInputStream.close();
			dos.flush();
			dos.close();

		}
		catch (MalformedURLException ex)
		{
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		catch (IOException ioe)
		{
			Log.e("Debug", "error: " + ioe.getMessage(), ioe);
		}

		//------------------ read the SERVER RESPONSE
		try {
			inStream = new DataInputStream ( conn.getInputStream() );
			String str;

			while (( str = inStream.readLine()) != null)
			{
				Log.e("Debug","Server Response "+str);
			}
			inStream.close();

		}
		catch (IOException ioex){
			Log.e("Debug", "error: " + ioex.getMessage(), ioex);
		}
		
		
	}
	
	/**
	 *  Will fetch the given picture from the webserver
	 */

	public Bitmap fetchPicture(String fileUrl)
	{
		//Stores the picture
		Bitmap bmImg = null;
		
		URL myFileUrl =null; 
		try {
			myFileUrl= new URL(fileUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn= (HttpURLConnection)myFileUrl.openConnection();
			conn.setDoInput(true);
			conn.connect();
			int length = conn.getContentLength();
			int[] bitmapData =new int[length];
			byte[] bitmapData2 =new byte[length];
			InputStream is = conn.getInputStream();

			bmImg = BitmapFactory.decodeStream(is);
			
		} catch (IOException e) {

			Log.e("log_tag", e.getMessage().toString());

		}
		
		return bmImg;
	}
	

	
}
