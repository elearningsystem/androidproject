package chat;


import java.util.ArrayList;
import java.util.List;

public class MessageThread {
	
	List<String> thread;
	private String ID = "";
	
	public MessageThread(String nID)
	{
		thread = new ArrayList<String>();
		this.ID = nID;
	}
	
	public void addMessage(String msg)
	{
		thread.add(msg);
	}
	
	public List<String> getThread()
	{
		return thread;
	}
	
	public String getUserID()
	{
		return ID;
	}

}
